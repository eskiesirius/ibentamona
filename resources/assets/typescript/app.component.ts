import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { LoginComponent }  from './auth/login.component';

@Component({
  selector: 'my-app',
  templateUrl: 'pages/app.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [ROUTER_PROVIDERS]
})

@RouteConfig([
  { path: '/login',  name: 'Login',  component: LoginComponent }
])

export class AppComponent { }