@extends('layouts.app')

@section('title', 'Ibenta Mo Na!')

@section('content')
<div class="ui huge breadcrumb header">
    <a class="section" href="/">Home</a>
    <i class="right chevron icon divider"></i>
    <div class="active section">User Panel</div>
</div>

<!-- Content -->
<div class="ui grid contents">
    <!-- Profile Header -->
    <div class="row">
        <div class="ui container segments">
            <div class="ui segment">
                <div class="ui grid">
                    <!-- User Login and Picture -->
                    <div class="twelve wide computer twelve wide tablet sixteen wide mobile column">
                        <div class="ui items ">
                            <div class="item profile">
                                <a class="ui tiny image">
                                    <img src="{{ asset(Auth::user()->profile_photo) }}">
                                </a>
                                <div class="content">
                                    <div class="header">Hi,</div> <a class="header">{{ Auth::user()->firstname . ' ' . Auth::user()->lastname }}</a>
                                    <div class="meta">
                                        <p>Your last login was: {{Auth::user()->last_login->diffForHumans()}}</p>
                                    </div>
                                </div>
                            </div>                                      
                        </div>
                    </div>

                    <!-- Ads Data -->
                    <div class="four wide computer four wide tablet sixteen wide mobile column">
                        <div class="ui statistics">
                            <div class="grey statistic">
                                <div class="value">
                                    {{Auth::user()->ads->count()}}
                                </div>
                                <div class="label">
                                    My Ads
                                </div>
                            </div>
                            <div class="red statistic">
                                <div class="value">
                                    {{Auth::user()->fave_ads->count()}}
                                </div>
                                <div class="label">
                                    Favorite Ads
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Profile Menu -->
            <div class="ui secondary segment">
                <div class="twelve wide computer twelve wide tablet sixteen wide mobile column">
                    <div class="ui horizontal list">
                        <div class="item">
                            <div class="content">
                                <a class="header" href="{{ url('/panel/my-ads') }}">
                                    My Ads
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <a class="header" href="{{ url('/panel/favorite-ads') }}">
                                    Favorite Ads
                                </a>
                            </div>
                        </div>
                        @if (Auth::user()->user_type == App\User::ADMIN_USER)
                        <div class="item">
                            <div class="content">
                                <a class="header" href="{{ url('/panel/users') }}">
                                    Users
                                </a>
                            </div>
                        </div>
                        @endif
                        <div class="item">
                            <div class="content">
                                <a class="header" href="{{ route('order-histories') }}">
                                    Order History
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <a class="header" href="{{url('/panel/profile')}}">
                                    Profile
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @yield('partial')

        <!-- Side Ads -->
        <div class="two wide computer only column">
            <div class="ui skyscraper test ad" data-text="Skyscraper"></div>
        </div>
    </div>
</div>

@section('js')

 $('.message .close')
.on('click', function() {
    $(this)
    .closest('.message')
    .transition('fade')
    ;
})
;

$('.ui.small.modal')
.modal('attach events', '.delete-ad', 'show')
;

$('.tag.icon')
.popup()
;

$('.delete.icon')
.popup()
;

$('table')
.tablesort()
;
@stop



@endsection
