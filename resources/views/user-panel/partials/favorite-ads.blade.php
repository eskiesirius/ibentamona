@extends('user-panel.panel')


@section('partial')
<div class="fourteen wide computer sixteen wide mobile column">
	<div class="ui segment">
		<h2 class="ui header">
			<div class="content">
				Favorite Ads
			</div>
		</h2>
		<div class="divider"></div>
		<div class="ui items divided">
			@foreach($ads as $index => $ad)
			<div class="item" id="fave_ad{{$index + 1}}">
				<a class="ui image ads" href="{{ url('/item/'.$ad->ad->slug) }}">
					@if ($ad->ad->ad_gallery != "")
					<img src="{{asset('/assets/img/ad/'.$ad->ad->ins_user.'/'.explode(',', $ad->ad->ad_gallery)[0])}}">
					@endif 	
				</a>
				<div class="content">
					<a class="header" href="{{ url('/item/'.$ad->ad->slug) }}">{{$ad->ad->ad_name}}</a>
					<div class="meta">
						<span>{{$ad->ad->category->cat_name}}</span>
					</div>
					<div class="description">
						<h4 class="ui green header">Price: ₱{{number_format($ad->ad->price)}} @if ($ad->ad->negotiable == '1')(Negotiable)@endif</h4>
					</div>
					<div class="ui divider"></div>
					<div class="extra">
						<span class="right floated">
							<i class="popup link tag icon" data-content="{{$ad->ad->condition_code}}" data-offset="-11"></i>
							<i class="popup link marker icon" data-content="{{$ad->ad->city->city_name}}" data-offset="-11"></i>
							<i class="popup link delete-ad delete red icon" id="remove_{{$index + 1}}" data-content="Remove Favorite" data-offset="-11" data-ad-id="{{ $ad->ad_id }}"></i>
						</span>
						<div>Created on: {{$ad->ad->approved_date}}</div>
						<div>Created by: <a href="#">{{$ad->ad->user->firstname. ' ' .$ad->ad->user->lastname}}</a></div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@include('pagination.custom',['paginator' => $ads])
	</div>
</div>

<form method="post" action="{{route('remove-fave-ad')}}" class="ui modal delete">
	{{ csrf_field() }}
	<div class="header">
		Remove Favorite Ad
	</div>

	<div class="content">
		<div class="description">
			Are you sure you want to remove this ad as your favorite? 
		</div>
	</div>
	<input type="hidden" id="ad_id" value="" name="ad_id"></input>
	<div class="actions">
		<div class="ui button negative">No</div>
		<button class="ui button positive" type="submit">Yes</button>
	</div>
</form>

@section('js')

$('.extra').on("click", ".delete-ad",function(e){
    e.preventDefault();
    var $this=$(this);
	var ad_id = $this.data('adId');
    var rowNumber = $this.data('index');
    $('#ad_id').val(ad_id);
	$('.ui.modal.delete').modal('show');
});


@include('alert.flash');

@stop

@endsection