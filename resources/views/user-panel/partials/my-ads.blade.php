@extends('user-panel.panel')


@section('partial')
<!-- My Ads -->
<div class="fourteen wide computer sixteen wide mobile column">
	<div class="ui segment">
		<h2 class="ui header">
			{{ csrf_field() }}
			<div class="content">
				My Ads
			</div>						
		</h2>
		<!-- TODO:: Search Ajax -->
		<div class="ui container right aligned">
			<div class="ui icon input">
				<input placeholder="Title,City or Category" type="text">
				<i class="circular search link icon"></i>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Table of your ads -->
		<table class="ui sortable celled table" style="overflow: auto;">
			<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Title</th>
					<th>Category</th>
					<th>Phone Number</th>
					<th>City</th>
					<th>Status</th>
					<th>Featured</th>
					<th>Days Left</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ads as $index => $ad)
				<tr>
					<td>
						{{$index + 1}}
					</td>
					<td>
						@if ($ad->ad_gallery != "")
						<img class="ui mini image grid row computer only tablet only" style="margin:auto" src="{{asset('/assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
						<!-- Mobile Display -->
						<img class="ui tiny image grid row mobile only" style="margin:auto" src="{{asset('/assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
						@endif 	
					</td>
					<td>{{$ad->ad_name}}</td>
					<td>{{$ad->category->cat_name}}</td>
					<td>{{$ad->phone}}</td>
					<td>{{$ad->city->city_name}}</td>
					<td id="status_{{$index + 1}}">
						<div class="ui {{ $ad->status == 0 ? ' yellow' : ' green' }} horizontal label">
							{{ $ad->status == 0 ? 'Pending' : 'Active' }}
						</div>
					</td>
					<td>
						<div class="ui blue horizontal label">
							{{ $ad->feat_flg == 0 ? 'No' : 'Yes' }}
						</div>
					</td>
					<td>
						@if ($ad->feat_flg == 0)
						--
						@elseif (\Carbon\Carbon::now()->diff($ad->feat_expiration)->days == 1)
						1 Day
						@elseif (\Carbon\Carbon::now()->diff($ad->feat_expiration)->days == 0)
						Now
						@else
						{{\Carbon\Carbon::now()->diff($ad->feat_expiration)->days}} Days
						@endif
					</td>
					<td>
						<div class="ui teal dropdown icon button table-action">
							<i class="setting icon"></i>Action
							<i class="dropdown icon"></i>
							<div class="menu">
								<a class="item" href="{{ url('/item/'.$ad->slug) }}"><i class="unhide icon"></i>View Post</a>
								@if (Auth::user()->user_type == App\User::ADMIN_USER && $ad->status == 0)
								<a class="item approve button" id="approve_{{$index + 1}}" data-ad-slug="{{ $ad->slug }}" data-index="{{$index + 1}}"><i class="check square icon"></i>Approve</a>
								@endif
								<a class="item" href="{{ url('/item/'.$ad->slug.'/edit') }}"><i class="edit icon"></i>Edit Post</a>
								<a class="item remove-ad" id="remove_{{$index + 1}}" data-ad-slug="{{ $ad->slug }}" data-index="{{$index + 1}}"><i class="delete icon"></i>Remove Post</a>
								@if ($ad->feat_flg == 0 && $ad->status == 1)
								<a class="item feature-ad" id="feature_{{$index + 1}}" data-ad-slug="{{ $ad->slug }}" data-index="{{$index + 1}}"><i class="announcement icon"></i>Feature Post</a>
								@elseif ($ad->status == 1)
								<a class="item renew-ad" id="renew_{{$index + 1}}" data-ad-slug="{{ $ad->slug }}" data-index="{{$index + 1}}"><i class="announcement icon"></i>Renew Featured Post</a>
								@endif
							</div>
						</div>
					</td>
				</tr>	
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th colspan="10">
						<!-- TODO: Change to AJAX -->
						@include('pagination.custom',['paginator' => $ads])
					</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<div class="ui modal feature">
	<div class="header">
		Payment
	</div>
	<div class="content">
		<div class="description">
			<div class="ui cards">
				<form method="POST" class="card" action="{{route('checkout')}}">
					{{ csrf_field() }}
					<div class="content">
						<div class="header">Package: 7 Days Feature</div>
						<div class="description">
							150php
						</div>
					</div>
					<input type="hidden" value="package1" name="package"></input>
					<input type="hidden" id="slug_1" value="" name="slug"></input>
					<button class="ui bottom attached button" type="submit">
						<i class="add icon"></i>
						Select this package
					</button>
				</form>
			</div>
		</div>
	</div>
</div>

<form method="post" action="{{route('remove-ad')}}" class="ui modal delete">
	{{ csrf_field() }}
	<div class="header">
		Remove Ad
	</div>

	<div class="content">
		<div class="description">
			Are you sure you want to remove this ad? 
		</div>
	</div>
	<input type="hidden" id="slug_delete" value="" name="slug"></input>
	<div class="actions">
		<div class="ui button negative">No</div>
		<button class="ui button positive" type="submit">Yes</button>
	</div>
</form>

@section('js')

$('.approve.button').click(function(e){
    e.preventDefault();
    var $this=$(this);
    var ad_slug = $this.data('adSlug');
    var rowNumber = $this.data('index');

    $.ajax({
        url: "/item/" + ad_slug + "/approve",
        type:'POST',
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        success: function(data){
            console.log(data);
            $this.remove();
            var statusChild = $('#status_' + rowNumber).children();
            console.log(statusChild);
            statusChild.removeClass("yellow");
            statusChild.addClass("green");
            statusChild.text("Active");	

            $('<a class="item feature-ad" id="feature_' + rowNumber + '" data-ad-slug="' + ad_slug + '" data-index="' + rowNumber + '"><i class="announcement icon"></i>Feature Post</a>').insertAfter('#remove_' + rowNumber);

            swal({
			  title: "Approved Ad!",
			  text: "Ad has been approved!",
			  type: "success",
			  timer: 1700,
			  showConfirmButton: false
			});
        },
        error: function(data){
            console.log(data);
        }
    });
});

$('.menu').on("click", ".item.renew-ad",function(e){
    e.preventDefault();
    var $this=$(this);
    showModal($this);
});

$('.menu').on("click", ".item.feature-ad",function(e){
    e.preventDefault();
    var $this=$(this);
    showModal($this);
});

function showModal($this)
{
    var ad_slug = $this.data('adSlug');
    var rowNumber = $this.data('index');
    $('#slug_1').val(ad_slug);
	$('.ui.modal.feature').modal('show');
}

$('.menu').on("click", ".item.remove-ad",function(e){
    e.preventDefault();
    var $this=$(this);
	var ad_slug = $this.data('adSlug');
    var rowNumber = $this.data('index');
    $('#slug_delete').val(ad_slug);
	$('.ui.modal.delete').modal('show');
});

@include('alert.flash');

@stop



@endsection