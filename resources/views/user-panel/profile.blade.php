@extends('user-panel.panel')


@section('partial')

<div class="fourteen wide computer sixteen wide mobile column">
	<div class="ui segment">
		<h2 class="ui header">
			<div class="content">
				Profile Information
			</div>
		</h2>
		@if (count($errors) > 0)
		<div class="ui error message">
			<i class="close icon"></i>
			<div class="header">
				There was some error/s with your submission
			</div>
			<ul class="list">
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="divider"></div>
		<form class="ui form" action="{{url('/panel/profile')}}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="two fields">
				<div class="required field{{ $errors->has('firstname') ? ' error' : '' }}">
					<label>First Name</label>
					<input placeholder="First Name" value="{{$user->firstname}}" name="firstname" type="text">
				</div>
				<div class="required field{{ $errors->has('lastname') ? ' error' : '' }}">
					<label>Last Name</label>
					<input placeholder="Last Name" value="{{$user->lastname}}" name="lastname" type="text">
				</div>
			</div>
			<div class="required field{{ $errors->has('phone') ? ' error' : '' }}">
				<label>Phone Number</label>
				<input type="text" name="phone" value="{{$user->phone}}" placeholder="09221111111">
			</div>
			<div class="required field{{ $errors->has('gender') ? ' error' : '' }}">
				<label>Gender</label>
				<div class="ui selection dropdown">
					<input name="gender" value="{{$user->gender}}" type="hidden">
					<i class="dropdown icon"></i>
					<div class="default text">Gender</div>
					<div class="menu">
						<div class="item" data-value="0">Male</div>
						<div class="item" data-value="1">Female</div>
					</div>
				</div>
			</div>
			<div class="field{{ $errors->has('profile_photo') ? ' error' : '' }}">
				<label>Upload Your Picture</label>
				<input type="hidden" value="{{$user->profile_photo}}" name="profile_photo">
				<input type="file" accept="image/*" name="file">
			</div>
			<div class="field">
				<label>Address</label>
				<input type="text" name="address" value="{{$user->address}}" placeholder="Address">
			</div>
			<button class="ui button positive" type="submit">Update Profile</button>
		</form>
		<br>
		@if (count($errors->PasswordError) > 0)
		<div class="ui error message">
			<i class="close icon"></i>
			<div class="header">
				There was some error/s with your submission
			</div>
			<ul class="list">
				@foreach ($errors->PasswordError->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<form class="ui form" action="{{url('/panel/profile/changepassword')}}" method="post">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<h2 class="ui header">
				<div class="content">
					Change Password
				</div>
			</h2>
			<div class="divider"></div>
			<div class="field required{{ $errors->PasswordError->has('password') ? ' error' : '' }}">
				<label>New Password</label>
				<input type="password" name="password" placeholder="Password">
			</div>
			<div class="field required{{ $errors->PasswordError->has('password_confirmation') ? ' error' : '' }}">
				<label>Confirm Password</label>
				<input type="password" placeholder="Confirm Password" name="password_confirmation">
			</div>
			<div class="field required{{ $errors->PasswordError->has('current_password') ? ' error' : '' }}">
				<label>Current Password</label>
				<input type="password" placeholder="Current Password" name="current_password">
			</div>
			<button class="ui button positive" type="submit">Update Password</button>
		</form>				
	</div>
</div>

@endsection

@section('js')
@include('alert.flash')
@endsection