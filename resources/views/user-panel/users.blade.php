@extends('user-panel.panel')


@section('partial')
<!-- Users -->
<div class="fourteen wide computer sixteen wide mobile column">
	{{ csrf_field() }}
	<div class="ui segment">
		<h2 class="ui header">
			<div class="content">
				All Users
			</div>						
		</h2>
		<!-- TODO:: Search Ajax -->
		<div class="ui container right aligned">
			<div class="ui icon input">
				<input placeholder="Title,City or Category" type="text">
				<i class="circular search link icon"></i>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Table of your ads -->
		<table class="ui sortable celled table" style="overflow: auto;">
			<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Gender</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $index => $user)
				<tr>
					<td>
						{{$index + 1}}
					</td>
					<td>
						<img class="ui mini image grid row computer only tablet only" style="margin:auto" src="{{asset($user->profile_photo)}}">
						<!-- Mobile Display -->
						<img class="ui tiny image grid row mobile only" style="margin:auto" src="{{asset($user->profile_photo)}}">	
					</td>
					<td>{{$user->firstname}}</td>
					<td>{{$user->email}}</td>
					<td>{{$user->phone}}</td>
					<td>{{$user->gender}}</td>
					<td id="status_{{$index + 1}}">
						<div class="ui {{ $user->status == 0 ? ' yellow' : ' green' }} horizontal label">
							{{ $user->status == 0 ? 'Pending' : 'Active' }}
						</div>
					</td>
					<td>
						<div class="ui teal dropdown icon button table-action">
							<i class="setting icon"></i>Action
							<i class="dropdown icon"></i>
							<div class="menu">
								<a class="item" href="{{url('/user/'.$user->user_id)}}"><i class="unhide icon"></i>View User</a>
								@if ($user->status == 0)
								<a class="item approve button" id="approve_{{$index + 1}}" data-id="{{ $user->user_id }}" data-index="{{$index + 1}}"><i class="check square icon"></i>Approve</a>
								@endif
								<a class="item remove-user" id="remove_{{$index + 1}}" data-id="{{ $user->user_id }}" data-index="{{$index + 1}}"><i class="delete icon"></i>Remove User</a>
							</div>
						</div>
					</td>
				</tr>	
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th colspan="10">
						<!-- TODO: Change to AJAX -->
						@include('pagination.custom',['paginator' => $users])
					</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<form method="post" action="{{route('remove-user')}}" class="ui modal delete">
	{{ csrf_field() }}
	<div class="header">
		Remove User
	</div>

	<div class="content">
		<div class="description">
			Are you sure you want to remove this user? 
		</div>
	</div>
	<input type="hidden" id="user_id" value="" name="user_id"></input>
	<div class="actions">
		<div class="ui button negative">No</div>
		<button class="ui button positive" type="submit">Yes</button>
	</div>
</form>


@section('js')
$('.approve.button').click(function(e){
    e.preventDefault();
    var $this=$(this);
    var userId = $this.data('id');
    var rowNumber = $this.data('index');

    $.ajax({
        url: "/user/" + userId + "/approve",
        type:'POST',
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        success: function(data){
            console.log(data);
            $this.remove();
            var statusChild = $('#status_' + rowNumber).children();
            console.log(statusChild);
            statusChild.removeClass("yellow");
            statusChild.addClass("green");
            statusChild.text("Active");

            swal({
			  title: "Approved user!",
			  text: "User has been approved!",
			  type: "success",
			  timer: 1700,
			  showConfirmButton: false
			});
        },
        error: function(data){
            console.log(data);
        }
    });
});

$('.menu').on("click", ".item.remove-user",function(e){
    e.preventDefault();
    var $this=$(this);
    var userId = $this.data('id');
    var rowNumber = $this.data('index');
    $('#user_id').val(userId);
	$('.ui.modal.delete').modal('show');
});

@include('alert.flash');

@stop



@endsection