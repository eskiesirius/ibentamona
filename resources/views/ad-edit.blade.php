@extends('layouts.app')

@section('title', 'Edit | Ibenta mo na yan!')

@section('content')
<style type="text/css">
    .dz-preview.dz-complete.dz-image-preview {
        z-index: 1;
    }
    .dz-default.dz-message{
        display: none;
    }
</style>

<div class="ui huge breadcrumb header">
    <a class="section">Home</a>
    <i class="right chevron icon divider"></i>
    <div class="active section">Ad Edit</div>
</div>

<!-- Content -->
<div class="ui grid two column contents">

    <!-- Forms -->
    <div class="eleven wide computer sixteen wide mobile column">
        <!-- Ad Information -->
        <div class="row">
            <div class="ui piled segment">
               <h2 class="ui dividing header">Ad Information Detail</h2>
               @if (count($errors) > 0)
               <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">
                    There was some error/s with your submission
                </div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="ui form large" method="post" action="{{ url('/item/'.$ad->slug.'/edit') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="ui error message"></div>
                <div class="required field {{ $errors->has('ad_name') ? ' error' : '' }}">
                    <label>Title of your Ad</label>
                    <input name="ad_name" placeholder="ex: Samsung s5 Brand New" type="text" value="{{ $ad->ad_name }}">
                </div>
                <div class="required field">
                    <label>Category</label>
                    <div class="ui dropdown search selection main ajax">
                        <input type="hidden" class="maincategory value" name="cat_id" value="{{ $ad->cat_id }}">
                        <div class="default text maincategory">Category</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            @foreach($maincategorylist as $maincategory)
                            <div class="item" data-value="{{$maincategory->cat_id}}">{{$maincategory->cat_name}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Sub Category</label>
                    <div class="ui dropdown search selection sub ajax">
                        <input type="hidden" class="subcategory value" name="sub_cat_id" value="{{ $ad->sub_cat_id }}">
                        <div id="subcattitle" class="default text">Sub Category</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            @foreach($subcategorylist as $subcat)
                            <div class="item @if($subcat->sub_cat_id == $ad->sub_cat_id) active selected @endif " data-value="{{$subcat->sub_cat_id}}">{{$subcat->sub_cat_name}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="required field">
                    <label>City</label>
                    <div class="ui dropdown search selection city ajax">
                        <input type="hidden" class="city value" name="city_id" value="{{ $ad->city_id }}">
                        <div class="default text">Please Select City</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            @foreach($citylist as $city)
                            <div class="item" data-value="{{$city->city_id}}">{{$city->city_name}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="required field fallback">
                    <label>Upload Ad's Images</label>
                    <input type="hidden" id="ad_gallery" name="ad_gallery" value="{{ $ad->ad_gallery }}" autocomplete="off">
                    <div id="upload_pic" class="dropzone">
                    </div>
                </div>
                <div class="required field">
                    <label>Condition</label>
                    <div class="inline fields">
                        <div class="field">
                            <div id="rbNew" class="ui radio checkbox">
                                <input class="hidden" value="0" tabindex="0" name="condition_code" checked="" type="radio">
                                <label>New</label>
                            </div>
                        </div>
                        <div class="field">
                            <div id="rbUsed" class="ui radio checkbox">
                                <input class="hidden" value="1" tabindex="0" name="condition_code" type="radio">
                                <label>Used</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="required field">
                    <label>Price</label>
                    <div class="inline field">
                        <input name="price" placeholder="₱" type="text" value="{{ $ad->price }}">
                        <div id="chkNegotiable" class="ui checkbox">
                            <input  class="hidden" tabindex="0" type="checkbox" name="negotiable" value="1">
                            <label>Negotiable</label>
                        </div>
                    </div>
                </div>
                <div class="required field">
                    <label>Phone Number</label>
                    <div class="inline field">
                        <div class="ui input" id="ad_phone">
                            <input name="phone" id="phone" placeholder="09221111111" type="text" value="{{ $ad->phone }}">
                        </div>
                        <div class="ui checkbox">
                            <input autocomplete="off" name="sameuser" class="hidden" id="same_user" tabindex="0" type="checkbox">
                            <label>Same as the User's Phone</label>
                        </div>
                    </div>
                </div>
                <div class="required field">
                    <label>Description</label>
                    <textarea name="description">{{ $ad->description }}</textarea>
                </div>
                <div class="field">
                    <label>Tags</label>
                    <div class="inline field">
                        <textarea name="tag">{{ $ad->tag }}</textarea>
                        <label>put as (,) comma sperated</label>
                    </div>
                </div>
                <button class="ui button positive" type="submit">Update my Ad</button>
            </form> 
        </div>
    </div>
</div>

<!-- Rules of Posting -->
<div class="five wide computer sixteen wide mobile column">
    <div class="row">
        <div class="ui raised segment">
            <h2 class="ui dividing header">Quick Rules</h2>
            <p>
                Posting an ad on ibentamona.com is free! However, all ads must follow our rules:
            </p>
            <div class="ui list">
                <div class="item">
                    <i class="right triangle icon"></i> 
                    <div class="content">
                        <div class="description">Make sure you post in the correct category.</div>
                    </div>
                </div>
                <div class="item">
                    <i class="right triangle icon"></i> 
                    <div class="content">
                        <div class="description">Do not post the same ad more than once or repost an ad within 48 hours.</div>
                    </div>
                </div>
                <div class="item">
                    <i class="right triangle icon"></i> 
                    <div class="content">
                        <div class="description">Do not upload pictures with watermarks.</div>
                    </div>
                </div>
                <div class="item">
                    <i class="right triangle icon"></i> 
                    <div class="content">
                        <div class="description">Do not post ads containing multiple items unless it's a package deal.</div>
                    </div>
                </div>
                <div class="item">
                    <i class="right triangle icon"></i> 
                    <div class="content">
                        <div class="description">Do not put your email or phone numbers in the title or description.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@section('js')
@if ($ad->condition_code == '0')
$('#rbNew').checkbox( 'set checked' );
@else
$('#rbUsed').checkbox( 'set checked' );
@endif

@if ($ad->negotiable == '0')
$('#chkNegotiable').checkbox( 'set unchecked' );
@else
$('#chkNegotiable').checkbox( 'set checked' );
@endif

function displaySubCatList(categoryid)
{  

    $('.ui.dropdown.sub.ajax')
    .dropdown('clear')
    .dropdown({
        apiSettings: {
            url: '/sub-category-list/' + categoryid,
            beforeSend: function(settings) {
                settings.urlData = {
                    maincat: $('.maincategory.value').val(),
                };
                return settings;
            },
        },
        saveRemoteData: false
    });    
}

$(".ui.dropdown.main").dropdown({
    onChange:function(value,text)
    {
        displaySubCatList(value);
    }
});

var fileList = new Array;
var i =0;
Dropzone.autoDiscover = false;
$('#upload_pic').dropzone({ 
    url: "/upload/ad_image",
    maxFiles: 6,
    maxFilesize: 3,
    addRemoveLinks: true,
    acceptedFiles: "image/jpeg,image/png",
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    },
    accept: function(file, done) {
        if (file.width < 256 || file.height < 256) {
            done("Invalid dimensions!");
        }
        else { done(); }
    },
    success: function (file, response) {
        var imgName = response;

        if (imgName != 'error'){
            if ($('#ad_gallery').val() == '')
                $('#ad_gallery').val(imgName);
            else
                $('#ad_gallery').val($('#ad_gallery').val() + ',' + imgName);
        }
    },

    // The setting up of the dropzone
    init:function() {
        var myDropzone = this;
        var index = 0;
        var filenames = '{{$ad->ad_gallery}}'.split(',');

        filenames.forEach(function(filename){
            var mockFile = { name: filename, size: 12345 };
            myDropzone.emit("addedfile", mockFile);
            myDropzone.emit("thumbnail", mockFile, "/assets/img/ad/{{$ad->ins_user}}/"+filename);
            myDropzone.emit("complete", mockFile);
            index++;
        });

        var existingFileCount = index; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
        myDropzone.on("maxfilesexceeded", function(file) { this.removeFile(file); });

        this.on("thumbnail", function(file) {
            if (file.width < 256 || file.height < 256) {
                this.removeFile(file);
                alert("Image should be above 250 x 250 ");
            }
        });

        this.on("success", function(file, serverFileName) {
            if (serverFileName == 'error')
                this.removeFile(file);
            else {
                fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };
                i++;
            }
        });

        this.on("removedfile", function(file) {
            var rmvFile = "";
            var isOld = 0;
            
            for(f=0;f<fileList.length;f++){
                if(fileList[f].fileName == file.name)
                {
                    rmvFile = fileList[f].serverFileName;
                }
            }

            filenames.forEach(function(filename){
                if (filename == file.name)
                    isOld = 1;
            });

            if (isOld == 1)
            {
                var ad_gallery_str = $('#ad_gallery').val();
                ad_gallery_str = ad_gallery_str.replace(file.name, '');
                ad_gallery_str = ad_gallery_str.replace(',,',',');

                if (ad_gallery_str.length == 1)
                    ad_gallery_str = '';

                $('#ad_gallery').val(ad_gallery_str);
                myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
            }

            if (rmvFile){
                $.ajax({
                    type: 'POST',
                    url: '/remove/ad_image',
                    data: { "fileList" : rmvFile },
                    dataType: 'html',
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    success: function(data){
                        var ad_gallery_str = $('#ad_gallery').val();
                        ad_gallery_str = ad_gallery_str.replace(data, '');
                        ad_gallery_str = ad_gallery_str.replace(',,',',');

                        if (ad_gallery_str.length == 1)
                            ad_gallery_str = '';

                        $('#ad_gallery').val(ad_gallery_str);
                    }
                });
            }
        });
    }
});

$('.ui.form')
.form({
fields: {
ad_name     : {rules: [
{
    type: 'empty',
},
{
    type: 'maxLength[100]',
    prompt: 'Please enter at most {ruleValue} characters'
}
]},
cat_id      : {rules: [
{
    type: 'empty',
}
]},
city_id     : {rules: [
{
    type: 'empty',
}
]},
ad_gallery  : {rules: [
{
    type: 'empty',
    prompt: 'Please upload some Image(Maximum of 6)'
}
]},
price       : {rules: [
{
    type: 'empty',
    prompt: 'Price must have a value'
},
{
    type: 'integer',
    prompt: 'Price must be a number'
},
{
    type: 'maxLength[100]',
    prompt: 'Please enter at most {ruleValue} characters'
}
]},
phone       : {rules: [
{
    type: 'empty',
    prompt: 'Phone must have a value'
},
{
    type: 'integer',
    prompt: 'Phone must be a number'
},
{
    type: 'exactLength[11]',
}
]},
description : {rules: [
{
    type: 'empty',
}
]}
}
})
;


$('.ui.radio.checkbox')
.checkbox()
;

$('.ui.checkbox')
.checkbox()
;

$('#same_user').change(function() {
$('#phone').val('{{ Auth::user()->phone }}');
$('#ad_phone').toggleClass('disabled');

});

$('.message .close')
.on('click', function() {
$(this)
.closest('.message')
.transition('fade')
;
})
;

@stop

@endsection
