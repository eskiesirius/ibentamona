@extends('layouts.app')

@section('title', 'Ibenta Mo Na!')

@section('content')
<div class="ui container center aligned">
    <h1 class="ui header main">
        World's Largest Classifieds Portal
    </h1>
    <h3 class="ui header">
        Search from over 15,00,000 classifieds & Post unlimited classifieds free!
    </h3>

    @include('layouts.partials.search')

    <div class="ui massive horizontal list">
        <a class="item">
            <i class="blue facebook icon"></i>
        </a>
        <a class="item">
            <i class="red google plus icon"></i>
        </a>
        <a class="item">
            <i class="blue twitter icon"></i>
        </a>
        <a class="item">
            <i class="red youtube icon"></i>
        </a>
    </div>
</div>

<!-- Content -->
<div class="ui grid contents">

    <!-- Side Ads -->
    <div class="two wide column computer only">
        <div class="ui skyscraper test ad" data-text="Skyscraper"></div>
    </div>

    <!-- Category -->
    <div class="twelve wide computer sixteen wide column">
        <div class="ui raised segment">
            <div class="row">
                <div class="ui container center aligned">
                    <div class="ui four cards doubling">
                        @foreach($maincategories as $category)
                        <a class="card" href="{{route('search-ads',['category' => $category->slug])}}">
                            <div class="image">
                                <img src="{{asset('assets/img/categories/'.$category->cat_image)}}" class="ui wireframe image">
                            </div>
                            <div class="extra">
                                {{$category->cat_name}}
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>

            <!-- Featured Ads -->
            <div class="row">
                <h2 class="ui header">
                    <i class="diamond icon"></i>
                    <div class="content">
                        Featured Ads
                    </div>
                </h2>
            </div>
            <div class="ui divider"></div>

            <!-- Featured Items -->
            <div class="row">
                <div id="owl-example" class="owl-carousel">
                    @foreach($featuredads as $ad)
                    <div class="ui special cards">
                        <div class="card">
                            <div class="blurring dimmable image">
                                <div class="ui dimmer">
                                    <div class="content">
                                        <div class="center">
                                            <a class="ui inverted button" href="{{ url('/item/'.$ad->slug) }}">Take a Look</a>
                                        </div>
                                    </div>
                                </div>
                                @if ($ad->ad_gallery != "")
                                  <img src="{{asset('assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}" class="ui wireframe image">
                                @endif  
                            </div>
                            <div class="content">
                                <a class="header" href="{{ url('/item/'.$ad->slug) }}">{{$ad->ad_name}}</a>
                                <div class="meta">
                                    <span class="date">{{$ad->category->cat_name}}</span>
                                </div>
                                <div class="description">
                                    <h4 class="ui green header">Price: ₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h4>
                                </div>
                            </div>
                            <div class="extra content">
                                <a>
                                    <i class="wait icon"></i>{{$ad->approved_date->diffForHumans()}}
                                </a>
                                <span class="right floated">
                                    <i class="marker icon" data-content="{{$ad->city->city_name}}" data-offset="-11"></i>
                                    <i class="tag icon feature" data-content="{{$ad->condition_code}}" data-offset="-11"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <!-- Ads Banner -->
            <div class="row">
                <div class="ui grid">
                    <!-- Computer Version Ads -->
                    <div class="row computer only">
                        <div class="ui leaderboard test ad centered" data-text="Leaderboard"></div>
                    </div>

                    <!-- Mobile Version Ads -->
                    <div class="row mobile only tablet only">
                        <div class="ui mobile leaderboard test ad centered" data-text="Leaderboard"></div>
                    </div>
                </div>
            </div>

            <!-- Trending Ads Tab -->
            <!-- TODO: Change to Ajax -->
            <div class="row">
                <div class="ui top attached tabular menu">
                    <h2 class="ui header">
                        <i class="area chart icon"></i>
                        <div class="content">
                            Trending Ads
                        </div>
                    </h2>
                    <div class="right menu">
                        <div class="ui grid">
                            <div class="row computer only tablet only" style="margin-bottom:0px;margin-right:13px;">
                                <a class="active item" data-tab="first">Recent Ads</a>
                                <a class="item" data-tab="second">Popular Ads</a>
                                <a class="item" data-tab="third">Hot Ads</a>
                            </div>
                            <div class="row mobile only" style="margin-bottom:0px;">
                                <div class="ui dropdown selection trending-ads">
                                    <div class="default text">Recent Ads</div>
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        <div class="item" data-tab="first">Recent Ads</div>
                                        <div class="item" data-tab="second">Popular Ads</div>
                                        <div class="item" data-tab="third">Hot Ads</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Recent Ads -->
                <div class="ui bottom attached active tab segment" data-tab="first">
                    <div class="ui items divided">
                        @foreach($recentads as $ad)
                        <div class="item">
                            <a class="ui image ads" href="{{ url('/item/'.$ad->slug) }}">
                                <div class="ui blue ribbon label">
                                    <i class="certificate icon"></i>New
                                </div>
                                @if ($ad->ad_gallery != "")
                                  <img src="{{asset('assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
                                @endif  
                            </a>
                            <div class="content">
                                <a class="header" href="{{ url('/item/'.$ad->slug) }}">{{$ad->ad_name}}</a>
                                <div class="meta">
                                    <span>{{$ad->category->cat_name}}</span>
                                </div>
                                <div class="description">
                                    <h4 class="ui green header">Price: ₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h4>
                                </div>
                                <div class="ui divider"></div>
                                <div class="extra">
                                    <span style="margin-right: 15px;"><i class="wait icon"></i>{{$ad->approved_date->diffForHumans()}}</span>
                                    <span><i class="tag icon"></i>{{$ad->condition_code}}</span> <!-- TODO: Fix Condition -->
                                    <span class="right floated">
                                        <i class="marker icon"></i>
                                        {{$ad->city->city_name}}
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <!-- Popular Ads -->
                <div class="ui bottom attached tab segment" data-tab="second">
                    <div class="ui items divided">
                        @foreach($popularads as $ad)
                        <div class="item">
                            <a class="ui image ads" href="{{ url('/item/'.$ad->slug) }}">
                                <div class="ui orange ribbon label">
                                    <i class="heart icon"></i>Popular
                                </div>
                                @if ($ad->ad_gallery != "")
                                  <img src="{{asset('assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
                                @endif
                            </a>
                            <div class="content">
                                <a class="header" href="{{ url('/item/'.$ad->slug) }}">{{$ad->ad_name}}</a>
                                <div class="meta">
                                    <span>{{$ad->category->cat_name}}</span>
                                </div>
                                <div class="description">
                                    <h4 class="ui green header">Price: ₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h4>
                                </div>
                                <div class="ui divider"></div>
                                <div class="extra">
                                    <span style="margin-right: 15px;"><i class="wait icon"></i>{{$ad->approved_date->diffForHumans()}}</span>
                                    <span><i class="tag icon"></i>{{$ad->condition_code}}</span> <!-- TODO: Fix Condition -->
                                    <span class="right floated">
                                        <i class="marker icon"></i>
                                        {{$ad->city->city_name}}
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <!-- Hot Ads --><!-- TODO: Fix Hot Ads -->
                <div class="ui bottom attached tab segment" data-tab="third">
                    <div class="ui items divided">
                        <div class="item">
                            <div class="ui image ads">
                                <div class="ui red ribbon label">
                                    <i class="fire icon"></i>Hot
                                </div>
                                <img src="semantic/dist/img/image.png">
                            </div>
                            <div class="content">
                                <a class="header">Toyota</a>
                                <div class="meta">
                                    <span>Cars & Vehicles</span>
                                </div>
                                <div class="description">
                                    <h4 class="ui green header">Price: ₱100 (Negotiable)</h4>
                                </div>
                                <div class="ui divider"></div>
                                <div class="extra">
                                    <span style="margin-right: 15px;">2016/05/27</span>
                                    <span><i class="tag icon"></i>Used</span>
                                    <span class="right floated">
                                        <i class="marker icon" data-content="Philippines" data-offset="-11"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="ui image ads">
                                <div class="ui red ribbon label">
                                    <i class="fire icon"></i>Hot
                                </div>
                                <img src="semantic/dist/img/image.png">
                            </div>
                            <div class="content">
                                <a class="header">Toyota</a>
                                <div class="meta">
                                    <span>Cars & Vehicles</span>
                                </div>
                                <div class="description">
                                    <h4 class="ui green header">Price: ₱100 (Negotiable)</h4>
                                </div>
                                <div class="ui divider"></div>
                                <div class="extra">
                                    <span style="margin-right: 15px;">2016/05/27</span>
                                    <span><i class="tag icon"></i>Used</span>
                                    <span class="right floated">
                                        <i class="marker icon" data-content="Philippines" data-offset="-11"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Side Ads -->
    <div class="two wide column computer only">
        <div class="ui skyscraper test ad" data-text="Skyscraper"></div>    
    </div>
</div>

@section('js')
$("#owl-example").owlCarousel({
items : 3,
navigationText : ["<",">"],
autoPlay : true,
stopOnHover : true,
});

$('.special.cards .image').dimmer({
on: 'hover'
});

$('.marker.icon')
.popup()
;

$('.tag.icon.feature')
.popup()
;

$('.menu .item')
.tab()
;
@stop

@endsection
