@extends('layouts.app')

@section('title', 'Search | Ibenta Mo Na!')

@section('content')
<div class="ui huge breadcrumb header">
	<a class="section" href="/">Home</a>
	<i class="right chevron icon divider"></i>
	<div class="section active">Category</div>
</div>
<div class="ui container center aligned">
	@include('layouts.partials.search')
</div>

<div class="ui container" style="margin-top:3em;">
	<!-- Banner -->
	<div class="ui grid">
		<!-- Computer Version Ads -->
		<div class="row computer only">
			<div class="ui leaderboard test ad centered" data-text="Leaderboard"></div>
		</div>

		<!-- Mobile Version Ads -->
		<div class="row mobile only tablet only">
			<div class="ui mobile leaderboard test ad centered" data-text="Leaderboard"></div>
		</div>
	</div>
</div>

<!-- Content -->
<div class="ui grid contents">

	<!-- Category List Accordion -->
	<div class="five wide computer sixteen wide mobile column">
		<div class="row">
			<div class="ui vertical accordion fluid menu borderless">
				<div class="item">
					<a class="title">
						<i class="dropdown icon"></i>
						<b>All Categories</b>
					</a>
					<div class="active content">
						<div class="accordion">
							@foreach ($maincategories as $maincategory)
							<div class="item">
								<a class="title @if ($parameters->category == $maincategory->slug)active @endif">
									<i class="{{$maincategory->semantic_icon}}"></i>
									@if ($maincategory->sub_categories->count() != 0)
									<i class="dropdown icon"></i>
									@endif
									{{$maincategory->cat_name}}
								</a>
								<div class="content @if ($parameters->category == $maincategory->slug)active @endif">
									<div class="accordion">
										@foreach ($maincategory->sub_categories as $subcategory)
										<div class="item">
											<a href="{{route('search-ads',array_replace(array_merge(Request::all(), [
											'subcategory'=> $subcategory->slug ,
											'category' => $maincategory->slug,
											])))}}" class="title">
											{{$subcategory->sub_cat_name}}
										</a>
									</div>
									@endforeach
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Conditions -->
	<div class="row">
		<div class="ui vertical accordion fluid menu borderless">
			<div class="item">
				<a class="title">
					<i class="dropdown icon"></i>
					<b>Conditions</b>
				</a>
				<div class="active content">
					<div class="accordion">
						<div class="ui form">
							<div class="grouped fields">
								<div class="field">
									<div id="chkNew" class="ui checkbox">
										<input name="condition[]" type="checkbox" value="0">
										<label>New</label>
									</div>
								</div>
								<div class="field">
									<div id="chkUsed" class="ui checkbox">
										<input name="condition[]" type="checkbox" value="1">
										<label>Used</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Price -->
	<div class="row">
		<div class="ui vertical accordion fluid menu borderless">
			<div class="item">
				<a class="title">
					<i class="dropdown icon"></i>
					<b>Price</b>
				</a>
				<div class="active content">
					<div class="accordion">
						<div class="ui form">
							<div class="two fields">
								<div class="field">
									<label>Min Price</label>
									<input name="min" placeholder="₱" type="text" value="{{Request::get('min')}}">
								</div>
								<div class="field">
									<label>Max Price</label>
									<input name="max" placeholder="₱" type="text" value="{{Request::get('max')}}">
								</div>
							</div>
							<!-- TODO: Error Message -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<button class="ui button yellow fluid search" type="button">Search Ad</button>
	</div>

	<!-- Banner Ads -->
	<div class="row">
		<div class="ui grid container center aligned">
			<div class="row computer only">
				<div class="ui vertical rectangle test ad" data-text="Vertical Rectangle"></div>
			</div>
		</div>
	</div>
</div>

<!-- Display Ads -->
<div class="eleven wide computer sixteen wide mobile column">
	<div class="ui raised segment" style="margin-bottom:20px;">
		<div class="row">
			<div class="ui container">
				<h3 class="ui header">
					Search Result Count: {{$ads->total()}}
				</h3>
				<div class="ui divider"></div>
				<div class="ui three cards stackable">
					@foreach($ads as $ad)
					<div class="card">
						<a class="ui image" href="{{route('ad-detail',$ad->slug)}}">
							@if ($ad->feat_flg == '1')
							<div class="ui yellow ribbon label">
								Featured
							</div>
							@endif
							@if ($ad->ad_gallery != "")
							<img src="{{asset('assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
							@endif
						</a>
						<div class="content">
							<a class="header" href="{{route('ad-detail',$ad->slug)}}">{{$ad->ad_name}}</a>
							<div class="meta">
								<div>{{$ad->category->cat_name}}</div>
							</div>
							<div class="description">
								<h4 class="ui green header">Price: ₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h4>
							</div>
						</div>
						<div class="extra content">
							<div>
								<span class="right floated">
									<i class="tag icon"></i>
									{{$ad->condition_code}}
								</span>
								<span>
									<i class="wait icon"></i>
									{{$ad->approved_date->diffForHumans()}}
								</span>
							</div>
							<div>
								<span>
									<i class="marker icon"></i>
									{{$ad->city->city_name}}
								</span>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>

		<!-- Pagination -->
		<div class="row">
			<div class="ui container center aligned"> 
				@include('pagination.custom',['paginator' => $ads])
			</div>
		</div>
	</div>
</div>

@section('js')
$('.marker.icon')
.popup()
;

@if (Request::has('condition'))
	@foreach (Request::get('condition') as $condition)
		@if ($condition == '0')
		$('#chkNew').checkbox( 'check' );
		@endif

		@if ($condition == '1')
		$('#chkUsed').checkbox( 'check' );
		@endif
	@endforeach
@endif

$('.search').click(function() {
	var queryString = '';
	var checkboxes = $('input[name*=condition]');

	//Create Parameter
	var title = getUrlParameter('title');
	if (title != undefined)
	{
		if (queryString == '')
			queryString += 'title=' + title;
		else
			queryString += '&title=' + title;
	}

	var category = getUrlParameter('category');
	if (category != undefined)
	{
		if (queryString == '')
			queryString += 'category=' + category;
		else
			queryString += '&category=' + category;
	}

	var subcategory = getUrlParameter('subcategory');
	if (subcategory != undefined)
	{
		if (queryString == '')
			queryString += 'subcategory=' + subcategory;
		else
			queryString += '&subcategory=' + subcategory;
	}

	for (var x = 0;x< checkboxes.length;x++)
	{
		if (checkboxes[x].checked)
		{
			if (queryString == '')
				queryString += 'condition[]=' + checkboxes[x].value;
			else
				queryString += '&condition[]=' + checkboxes[x].value;
		}
	}

	var min = $('input[name=min]').val();
	var max = $('input[name=max]').val();

	if (queryString == '')
	{
		queryString += 'min=' + min;
		queryString += '&max=' + max;
	}
	else
	{
		queryString += '&min=' + min;
		queryString += '&max=' + max;
	}

	window.location.href = '/item?' + queryString;
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

@stop
@endsection