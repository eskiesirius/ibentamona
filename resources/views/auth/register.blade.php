@extends('layouts.app')

@section('title', 'Registration')

@section('content')
<div class="ui grid three column contents">
    <div class="row">
        <div class="four wide computer column">

        </div>
        <div class="eight wide computer sixteen wide mobile column">
            <div class="ui attached message">
                <div class="header">
                    <h1 class="ui header green">Welcome to our site!</h1>
                </div>
                <p>Fill out the form below to sign-up for a new account</p>
                @if (count($errors) > 0)
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        There was some error/s with your submission
                    </div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <form class="ui form attached fluid segment" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
                <div class="required field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>Email Address</label>
                    <input placeholder="Email" name="email" type="text" value="{{ old('email') }}">
                </div>
                <div class="required field {{ $errors->has('password') ? ' error' : '' }}">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password">
                </div>
                <div class="required field {{ $errors->has('password_confirmation') ? ' error' : '' }}">
                    <label>Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" name="password_confirmation">
                </div>
                <div class="two fields">
                    <div class="required field {{ $errors->has('firstname') ? ' error' : '' }}">
                        <label>First Name</label>
                        <input placeholder="First Name" name="firstname" type="text" value="{{ old('firstname') }}">
                    </div>
                    <div class="required field {{ $errors->has('lastname') ? ' error' : '' }}">
                        <label>Last Name</label>
                        <input placeholder="Last Name" name="lastname" type="text" value="{{ old('lastname') }}">
                    </div>
                </div>
                <div class="required field {{ $errors->has('phone') ? ' error' : '' }}">
                    <label>Phone Number</label>
                    <input type="text" name="phone" placeholder="09221111111" value="{{ old('phone') }}">
                </div>
                <div class="required field {{ $errors->has('gender') ? ' error' : '' }}">
                    <label>Gender</label>
                    <div class="ui selection dropdown">
                        <input name="gender" type="hidden" value="{{ old('gender') }}">
                        <i class="dropdown icon"></i>
                        <div class="default text">Gender</div>
                        <div class="menu">
                            <div class="item" data-value="0">Male</div>
                            <div class="item" data-value="1">Female</div>
                        </div>
                    </div>
                </div>        

                <!-- TODO: Captcha -->
                <div class="inline field required {{ $errors->has('terms') ? ' error' : '' }}">
                    <div class="ui checkbox">
                        <input id="terms" type="checkbox" name="terms">
                        <label for="terms">I agree to the terms and conditions</label>
                    </div>
                </div>
                <button class="ui button positive" type="submit">Sign Up</button>
            </form>
        </div>
        <div class="four wide computer column">

        </div>
    </div>
</div>
@endsection
