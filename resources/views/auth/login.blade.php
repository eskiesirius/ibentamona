@extends('layouts.app')

@section('title', 'Log In')

@section('content')
<div class="ui grid three column contents">
    <div class="row">
        <div class="four wide computer column">

        </div>
        <div class="eight wide computer sixteen wide mobile column">
            <div class="ui attached message">
                <div class="header">
                    <h1 class="ui header green">Welcome to our site!</h1>
                </div>
                <p>Fill out the form below to sign-in</p>
                @if (count($errors) > 0)
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        There was some error/s with your submission
                    </div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('warning'))
                <div class="ui warning message">
                    <i class="close icon"></i>
                    <div class="header">
                        Email Confirmation
                    </div>
                    {{ session('warning') }}
                </div>
                @endif
                @if (session('success') || session('status'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        @if (session('success'))
                        Account Activated
                        @endif
                        @if (session('status'))
                        Email Sent
                        @endif
                    </div>
                    {{ session('success') }}
                    {{ session('status') }}
                </div>
                @endif
                @if (session('negative'))
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        Something is wrong!
                    </div>
                    {{ session('negative') }}
                </div>
                @endif
            </div>
            <form class="ui form attached fluid segment" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>Email</label>
                    <input placeholder="Email" type="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="field {{ $errors->has('password') ? ' error' : '' }}">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password">
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input id="rememberme" type="checkbox" name="remember">
                        <label for="rememberme">Remember Me</label>
                    </div>
                </div>
                <button class="ui blue button" type="submit">Log In</button>
            </form>
            <div class="ui bottom attached warning message">
                <i class="icon help"></i>
                Not yet a Member? <a href="{{ url('/register') }}">Register here</a> instead. <a href="{{ url('/password/reset') }}">Forgot Password?</a>
            </div>
        </div>
        <div class="four wide computer column">

        </div>
    </div>
</div>

@section('js')
@include('alert.flash')

$('.message .close')
    .on('click', function() {
    $(this)
    .closest('.message')
    .transition('fade')
    ;
    })
;
@stop

@endsection
