@extends('layouts.app')

@section('title', 'Password Reset')

<!-- Main Content -->
@section('content')
<div class="ui grid three column contents">
    <div class="row">
        <div class="four wide computer column">

        </div>
        <div class="eight wide computer sixteen wide mobile column">
            <div class="ui attached message">
                <div class="header">
                    <h1 class="ui header green">Password Reset</h1>
                </div>
                <p>Fill out the form below with your Email to reset your password</p>
                @if ($errors->has('email'))
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        There was some error with your submission
                    </div>
                    <ul class="list">
                        @if ($errors->has('email'))
                        <li>{{ $errors->first('email') }}</li>
                        @endif
                    </ul>
                </div>
                @endif
            </div>
            <form class="ui form attached fluid segment" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="required field{{ $errors->has('email') ? ' error' : '' }}">
                    <label>Email Address</label>
                    <input placeholder="Email" type="email" name="email" value="{{ old('email') }}">
                </div>
                <!-- TODO: Captcha -->
                <button class="ui button positive" type="submit">Send Password Reset Link</button>
            </form>
        </div>
        <div class="four wide computer column">

        </div>
    </div>
</div>
@endsection

@section('js')
@include('alert.flash')
@endsection
