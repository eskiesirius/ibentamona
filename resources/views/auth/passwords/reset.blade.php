@extends('layouts.app')

@section('title', 'Password Reset')

@section('content')
<div class="ui grid three column contents">
    <div class="row">
        <div class="four wide computer column">

        </div>
        <div class="eight wide computer sixteen wide mobile column">
            <div class="ui attached message">
                <div class="header">
                    <h1 class="ui header green">Password Reset</h1>
                </div>
                <p>Fill out the form below to change your Password</p>
                @if ($errors->has('email') || 
                $errors->has('password') ||
                $errors->has('password_confirmation'))
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        There was some error/s with your submission
                    </div>
                    <ul class="list">
                        @if ($errors->has('email'))
                        <li>{{ $errors->first('email') }}</li>
                        @endif
                        @if ($errors->has('password'))
                        <li>{{ $errors->first('password') }}</li>
                        @endif
                        @if ($errors->has('password_confirmation'))
                        <li>{{ $errors->first('password_confirmation') }}</li>
                        @endif
                    </ul>
                </div>
                @endif
            </div>
            <form class="ui form attached fluid segment" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="required field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>Email Address</label>
                    <input placeholder="Email" name="email" type="text" value="{{ old('email') }}">
                </div>
                <div class="required field {{ $errors->has('password') ? ' error' : '' }}">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password">
                </div>
                <div class="required field {{ $errors->has('password_confirmation') ? ' error' : '' }}">
                    <label>Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" name="password_confirmation">
                </div>
                <button class="ui button positive" type="submit">Sign Up</button>
            </form>
        </div>
        <div class="four wide computer column">

        </div>
    </div>
</div>
@endsection

@section('js')
$('.message .close')
.on('click', function() {
    $(this)
    .closest('.message')
    .transition('fade')
    ;
})
;
@endsection