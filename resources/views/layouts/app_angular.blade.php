<!DOCTYPE html>
<html>
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <link rel="stylesheet" type="text/css" href="{{asset('semantic/dist/semanticnew.css')}}">

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{asset('semantic/dist/owl-carousel/owl.carousel.css')}}">

    <!-- Default Theme -->
    <link rel="stylesheet" href="{{asset('semantic/dist/owl-carousel/owl.theme.css')}}">

    <!-- Main Theme -->
    <link rel="stylesheet" href="{{asset('semantic/dist/css/main.css')}}">

    <!-- Polyfill(s) for older browsers -->
    <script src="angular_files/core-js/client/shim.min.js"></script>
    <script src="angular_files/zone.js/dist/zone.js"></script>
    <script src="angular_files/reflect-metadata/Reflect.js"></script>
    <script src="angular_files/systemjs/dist/system.src.js"></script>
    <!-- 2. Configure SystemJS -->  
    <script src="angular_files/systemjs.config.js"></script>
    <script>
    System.import('angular_files/typescript/boot').catch(function(err){ console.error(err); });
    </script>

    <title>@yield('title')</title>
</head>
<body>

    <my-app>Loading...</my-app>

    <!-- Include js plugin -->
    <script src="{{asset('semantic/dist/jquery.min.js')}}"></script>
    <script src="{{asset('semantic/dist/owl-carousel/owl.carousel.js')}}"></script>
    <script src="{{asset('semantic/dist/semantic.min.js')}}"></script>
    <script src="{{asset('semantic/dist/datepicker.js')}}"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $('.ui.sidebar.menu')
        .sidebar('attach events', '.item.hamburger')
        ;

        $("#owl-example").owlCarousel({
            items : 3,
            navigationText : ["<",">"],
            autoPlay : true,
            stopOnHover : true,
        });

        $('.special.cards .image').dimmer({
            on: 'hover'
        });

        $('.marker.icon')
        .popup()
        ;

        $('.menu .item')
        .tab()
        ;

        $('.ui.dropdown')
        .dropdown()
        ;

        $('.ui.accordion')
        .accordion()
        ;

        @section('js')

        @show
    });
    </script>
</body>
</html>