<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <link rel="stylesheet" type="text/css" href="{{asset('semantic/dist/semanticnew.css')}}">

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{asset('semantic/dist/owl-carousel/owl.carousel.css')}}">

    <!-- Default Theme -->
    <link rel="stylesheet" href="{{asset('semantic/dist/owl-carousel/owl.theme.css')}}">

    <!-- Main Theme -->
    <link rel="stylesheet" href="{{asset('semantic/dist/css/main.css')}}">

    <!-- DropZone Theme -->
    <link rel="stylesheet" href="{{asset('semantic/dist/css/dropzone.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('semantic/dist/css/sweetalert.css')}}">

    <!-- Polyfill(s) for older browsers -->
    <!-- <script src="angular_files/core-js/client/shim.min.js"></script>
    <script src="angular_files/zone.js/dist/zone.js"></script>
    <script src="angular_files/reflect-metadata/Reflect.js"></script>
    <script src="angular_files/systemjs/dist/system.src.js"></script> -->
    <!-- 2. Configure SystemJS -->  
    <!-- <script src="angular_files/systemjs.config.js"></script>
    <script>
    System.import('angular_files/typescript/boot').catch(function(err){ console.error(err); });
</script> -->

<title>@yield('title')</title>
</head>
<body>
    <!-- Sidebar Menu -->
    <div class="ui sidebar vertical labeled icon menu">
        <a class="item" href="{{ url('/') }}">
            <i class="home icon"></i>
            Home
        </a>
        <a class="item" href="{{route('search-ads')}}">
            <i class="block layout icon"></i>
            Category
        </a>
        @if (Auth::guest())
        <a class="item" href="{{ url('/login') }}">
            <i class="user icon"></i>
            Login/Register
        </a>
        @else
        <div class="item">
            <div class="ui accordion">
                <div class="title">
                    <i class="dropdown icon"></i>
                    <img class="ui avatar image" src="{{Auth::user()->profile_photo}}">
                    {{ Auth::user()->firstname }}
                </div>
                <div class="content">
                    <div class="menu">
                        <a class="item" href="{{ url('/panel/my-ads') }}">
                            <i class="tasks icon"></i>
                            My Ads
                        </a>
                        <a class="item" href="{{ url('/panel/favorite-ads') }}">
                            <i class="heart icon"></i>
                            Favorite Ads
                        </a>
                        <a class="item" href="{{url('/panel/profile')}}">
                            <i class="wrench icon"></i>
                            Change Profile
                        </a>
                        <div class="divider"></div>
                        <a class="item" href="{{ url('/logout') }}">
                            <i class="sign out icon"></i>
                            Sign out
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="item">
            <a class="ui primary button" href="{{ url('/posting') }}">Post Your Ad</a>
        </div>
    </div>  
    
    <div class="pusher">
        <div class="ui grid">
            <!-- Menu Computer View -->
            <div class="row computer only">
                <div class="ui menu large borderless fixed">
                    <a class="item" href="{{ url('/') }}">Home</a>
                    <a class="item" href="{{route('search-ads')}}">Category</a>
                    <div class="right menu">
                        @if (Auth::guest())
                        <a class="item" href="{{ url('/login') }}">
                            <i class="user icon"></i>
                            Login/Register
                        </a>    
                        @else
                        <div class="item">
                            <div class="ui inline dropdown">
                                <div class="text">
                                    <img class="ui avatar image" src="{{asset(Auth::user()->profile_photo)}}">
                                    {{ Auth::user()->firstname }}
                                </div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <a class="item" href="{{ url('/panel/my-ads') }}">
                                        <i class="tasks icon"></i>
                                        My Ads
                                    </a>
                                    <a class="item" href="{{ url('/panel/favorite-ads') }}">
                                        <i class="heart icon"></i>
                                        Favorite Ads
                                    </a>
                                    <a class="item" href="{{url('/panel/profile')}}">
                                        <i class="wrench icon"></i>
                                        Change Profile
                                    </a>
                                    <div class="divider"></div>
                                    <a class="item" href="{{ url('/logout') }}">
                                        <i class="sign out icon"></i>
                                        Sign out
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="item">
                            <a class="ui primary button" href="{{ url('/posting') }}">Post Your Ad</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Menu Mobile and Tablet View -->
            <div class="row mobile only tablet only">
                <div class="ui top fixed borderless large demo menu">
                    <a class="item hamburger">
                        <i class="sidebar icon"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="ui vertical masthead segment">
            <div class="ui container">
                @yield('content')
            </div>
        </div>

        <!-- Footer -->
        <div class="ui black inverted vertical footer segment">
            <div class="ui center aligned container">
                <div class="ui stackable inverted grid">
                    <div class="three wide column">
                        <h2 class="ui inverted header">Quick Links</h2>
                        <div class="ui inverted huge link list">
                            <a target="_blank" href="#" class="item">About Us</a>
                            <a target="_blank" href="#" class="item">Contact Us</a>
                            <a target="_blank" href="#" class="item">Privacy Policy</a>
                            <a target="_blank" href="#" class="item">Term and Conditions</a>
                        </div>
                    </div>
                    <div class="three wide column">
                        <h2 class="ui inverted header">Follow Us On</h2>
                        <div class="ui inverted massive link list">
                            <a class="item">
                                <i class="facebook square icon"></i>
                            </a>
                            <a class="item">
                                <i class="google plus square icon"></i>
                            </a>
                            <a class="item">
                                <i class="twitter square icon"></i>
                            </a>
                            <a class="item">
                                <i class="youtube square icon"></i>
                            </a>
                        </div>
                    </div>
                    <div class="seven wide right floated column">
                        <h2 class="ui inverted teal header">Help Preserve This Project</h2>
                        <p> Support for the continued development of Semantic UI comes directly from the community.</p>
                        <form target="_top" method="post" action="https://www.paypal.com/cgi-bin/webscr">
                            <input type="hidden" value="_s-xclick" name="cmd">
                            <input type="hidden" value="7ZAF2Q8DBZAQL" name="hosted_button_id">
                            <button class="ui large teal button" type="submit">Donate Today</button>
                        </form>
                    </div>
                </div>
                <div class="ui inverted section divider"></div>
                <img class="ui centered mini image" src="semantic/dist/img/logo.png">
                <div class="ui horizontal inverted small divided link list">
                    <a target="_blank" href="http://semantic-ui.mit-license.org/" class="item">Free &amp; Open Source (MIT)</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Include js plugin -->
    <script src="{{asset('semantic/dist/jquery.min.js')}}"></script>
    <script src="{{asset('semantic/dist/owl-carousel/owl.carousel.js')}}"></script>
    <script src="{{asset('semantic/dist/semantic.min.js')}}"></script>
    <script src="{{asset('semantic/dist/datepicker.js')}}"></script>
    <script src="{{asset('semantic/dist/dropzone.js')}}"></script>
    <script src="{{asset('semantic/dist/components/table.js')}}"></script>
    <script src="{{asset('semantic/dist/sweetalert-dev.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.ui.sidebar.menu')
            .sidebar('attach events', '.item.hamburger')
            ;

            $("#owl-example").owlCarousel({
                items : 3,
                navigationText : ["<",">"],
                autoPlay : true,
                stopOnHover : true,
            });

            $('.special.cards .image').dimmer({
                on: 'hover'
            });

            $('.marker.icon')
            .popup()
            ; 

            $('.menu .item')
            .tab()
            ;

            $('.ui.dropdown')
            .dropdown()
            ;

            $('.ui.accordion')
            .accordion()
            ;

            @section('js')

            @show
        });
    </script>
</body>
</html>