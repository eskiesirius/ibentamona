<form class="ui form" action="{{route('search-ads')}}" method="get">
    <div class="ui raised segment">
        <div class="three fields">
            <div class="field five wide">
                <div class="ui dropdown selection">
                    <input type="hidden" class="maincategory value" value="{{Request::get('category')}}" name="category">
                    <div class="default text">Category</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        @foreach($maincategories as $maincategory)
                        <div class="item" data-value="{{$maincategory->slug}}">{{$maincategory->cat_name}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="field eleven wide">
                <input placeholder="Ano Hanap mo?" name="title" value="{{Request::get('title')}}" type="text">
            </div>
            <div class="field">
                <button class="ui button fluid" type="submit">Hanapin na!</button>
            </div>
        </div>                      
    </div>
</form>