@extends('user-panel.panel')


@section('partial')
<div class="fourteen wide computer sixteen wide mobile column">
	<div class="ui segment">
		<h2 class="ui header">
			{{ csrf_field() }}
			<div class="content">
				Order Histories
			</div>						
		</h2>
		<!-- TODO:: Search Ajax -->
		<div class="ui container right aligned">
			<div class="ui icon input">
				<input placeholder="Title,City or Category" type="text">
				<i class="circular search link icon"></i>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Table of your ads -->
		<table class="ui sortable celled table" style="overflow: auto;">
			<thead>
				<tr>
					<th>#</th>
					<th>Payment ID</th>
					<th>Invoice Number</th>
					<th>Package Name</th>
					<th>Ad Name</th>
					<th>Status</th>
					<th>Ordered Date</th>
					<th>Approved Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach($orders as $index => $order)
				<tr>
					<td>
						{{$index + 1}}
					</td>
					<td>
						{{$order->payment_id}}
					</td>
					<td>{{$order->invoice_number}}</td>
					<td>{{$order->package_id}}</td>
					<td>{{$order->ad->ad_name}}</td>
					<td id="status_{{$index + 1}}">
						<div class="ui 
						@if ($order->status == 0)
						yellow
						@elseif ($order->status == 1)
						green
						@else
						red
						@endif
						 horizontal label">
						@if ($order->status == 0)
						Pending
						@elseif ($order->status == 1)
						Paid
						@else
						Cancelled
						@endif
						</div>
					</td>
					<td>
						{{ $order->created_at->format('Y-m-d') }}
					</td>
					<td>
						@if ($order->status == 1)
						{{ $order->approved_date->format('Y-m-d') }}
						@else
						--
						@endif
					</td>
				</tr>	
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th colspan="10">
						<!-- TODO: Change to AJAX -->
						@include('pagination.custom',['paginator' => $orders])
					</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

@section('js')

@include('alert.flash');

@stop



@endsection