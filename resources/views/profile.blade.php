@extends('layouts.app')

@section('title', 'Ibenta Mo Na!')

@section('content')
<div class="ui huge breadcrumb header">
	<a class="section" href="/">Home</a>
	<i class="right chevron icon divider"></i>
	<div class="active section">User</div>
</div>

<!-- Content -->
<div class="ui grid contents">
	<!-- Profile Header -->
	<div class="row">
		<div class="ui container segments">
			<div class="ui segment">
				<div class="ui grid">
					<!-- User Login and Picture -->
					<div class="twelve wide computer twelve wide tablet sixteen wide mobile column">
						<div class="ui items ">
							<div class="item profile">
								<a class="ui tiny image">
									<img src="{{ asset($user->profile_photo) }}">
								</a>
								<div class="content">
									<div class="header">{{ $user->firstname . ' ' . $user->lastname }}</div>
									<div class="meta">
										<p>Gender: {{ $user->gender }}</p>
										<p>Last login was: {{$user->last_login->diffForHumans()}}</p>
									</div>
								</div>
							</div>                                      
						</div>
					</div>

					<!-- Ads Data -->
					<div class="four wide computer four wide tablet sixteen wide mobile column">
						<div class="ui statistics">
							<div class="grey statistic">
								<div class="value">
									{{$ads->count()}}
								</div>
								<div class="label">
									Ads
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="fourteen wide computer sixteen wide mobile column">
			<div class="ui segment">
				<h2 class="ui header">
					<div class="content">
						User Ads
					</div>
				</h2>
				<div class="divider"></div>
				<div class="ui items divided">
					@foreach($ads as $ad)
					<div class="item">
						<a class="ui image ads" href="{{ url('/item/'.$ad->slug) }}">
							@if ($ad->ad_gallery != "")
							<img src="{{asset('/assets/img/ad/'.$ad->ins_user.'/'.explode(',', $ad->ad_gallery)[0])}}">
							@endif 	
						</a>
						<div class="content">
							<a class="header" href="{{ url('/item/'.$ad->slug) }}">{{$ad->ad_name}}</a>
							<div class="meta">
								<span>{{$ad->category->cat_name}}</span>
							</div>
							<div class="description">
								<h4 class="ui green header">Price: ₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h4>
							</div>
							<div class="ui divider"></div>
							<div class="extra">
								<span class="right floated">
									<i class="popup link tag icon" data-content="{{$ad->condition_code}}" data-offset="-11"></i>
									<i class="popup link marker icon" data-content="{{$ad->city->city_name}}" data-offset="-11"></i>
								</span>
								<div>Created on: {{$ad->approved_date}}</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>

		<!-- Side Ads -->
		<div class="two wide computer only column">
			<div class="ui skyscraper test ad" data-text="Skyscraper"></div>
		</div>
	</div>
</div>

@section('js')
$('table')
.tablesort()
;


$('.ui.small.modal')
.modal('attach events', '.delete-ad', 'show')
;

$('.tag.icon')
.popup()
;

$('.delete.icon')
.popup()
;

@stop



@endsection
