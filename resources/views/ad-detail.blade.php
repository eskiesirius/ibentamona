@extends('layouts.app')

@section('title', $ad->ad_name . ' for sale')

@section('content')
<div class="ui huge breadcrumb header">
    <a class="section" href="/">Home</a>
    <i class="right chevron icon divider"></i>
    <a class="section">Category</a>
    <i class="right chevron icon divider"></i>
    <a class="section active">{{$ad->category->cat_name}}</a>
    <i class="right chevron icon divider"></i>
    <div class="section active">Phones</div> <!-- TODO: Sub Category -->
</div>
<div class="ui container center aligned">
    @include('layouts.partials.search')
</div>

<div class="ui container" style="margin-top:3em;">
    <!-- Banner -->
    <div class="ui grid">
        <!-- Computer Version Ads -->
        <div class="row computer only">
            <div class="ui leaderboard test ad centered" data-text="Leaderboard"></div>
        </div>

        <!-- Mobile Version Ads -->
        <div class="row mobile only tablet only">
            <div class="ui mobile leaderboard test ad centered" data-text="Leaderboard"></div>
        </div>
    </div>
</div>

<!-- Content Detail -->
<div class="ui grid two column contents">
    <!-- Side Ads -->
    <div class="two wide column computer only">
        <div class="ui skyscraper test ad" data-text="Skyscraper"></div>
    </div>

    <div class="fourteen wide computer sixteen wide column">
        <!-- Picture and Ad Information -->
        <div class="row">
            <div class="ui grid two column">
                <div class="ten wide computer sixteen wide column">
                    <!-- Picture -->
                    <div class="ui raised segment">
                        <div id="ad-detail" class="owl-carousel">
                            @foreach(explode(',', $ad->ad_gallery) as $pic) 
                            <img src="{{asset('assets/img/ad/'. $ad->ins_user.'/'.$pic)}}" class="ui large wireframe image" style="margin: auto;">
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="six wide computer sixteen wide column">
                    <!-- Ad Information -->
                    <div class="ui raised segment">
                        <h2 class="ui green header">₱{{number_format($ad->price)}} @if ($ad->negotiable == '1')(Negotiable)@endif</h2>
                        <h1 class="ui header">{{$ad->ad_name}}</h1>
                        <p>Offered by: <a href="{{url('/user/'.$ad->user->user_id)}}">{{$ad->user->firstname . ' ' . $ad->user->lastname}}</a></p>
                        <p>Ad ID: {{$ad->ad_id}}</p>
                        <p>
                            <i class="wait icon"></i>
                            {{$ad->approved_date}}
                        </p>
                        <p>
                            <i class="marker icon"></i>
                            {{$ad->city->city_name}}
                        </p>
                        <p>
                            <i class="tag icon"></i>
                            {{$ad->condition_code}}
                        </p>
                        <button class="ui contact orange button">
                            <i class="phone square icon"></i>
                            Click to Show Phone Number
                        </button>
                        <h3 class="ui header">Share This Ad</h3>
                        <div class="ui horizontal list">
                            <a href="#" class="item">
                                <i class="blue facebook square icon large"></i>
                            </a>
                            <a href="#" class="item">
                                <i class="blue twitter square icon large"></i>
                            </a>
                            <a href="#" class="item">
                                <i class="red google plus square icon large"></i>
                            </a>
                            <a href="#" class="item">
                                <i class="blue linkedin square icon large"></i>
                            </a>
                            <a href="#" class="item">
                                <i class="red pinterest square icon large"></i>
                            </a>
                        </div>
                        <p></p>
                        @if(Auth::Guest() || $ad->ins_user != Auth::user()->user_id)
                        @if (!Auth::Guest())
                        <p>
                            @if($ad->ad_favorites->where('user_id',Auth::user()->user_id)->count() != 0)
                            <a class="favorited" data-ad-slug="{{ $ad->slug }}" id="saveAdFavorite" href="#">
                                <i class="red heart icon"></i>
                                Remove Ad as Favorite
                            </a>
                            @else
                            <a data-ad-slug="{{ $ad->slug }}" id="saveAdFavorite" href="#">
                                <i class="red empty heart icon"></i>
                                Save Ad as Favorite
                            </a>
                            @endif 
                        </p>
                        @endif
                        <p>
                            <a data-ad-slug="{{ $ad->slug }}" id="reportAd" href="#">
                                <i class="yellow warning sign icon"></i>
                                Report this Ad
                            </a>
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Description -->
        <div class="row">
            <div class="ui raised segment">
                <h3 class="ui header">Description</h3>
                <p>
                    {!! nl2br(e($ad->description)) !!}
                </p>
            </div>
        </div>

        <!-- Send Message to Seller -->
        <!-- TODO: Send Message Function -->
        @if(Auth::Guest() || $ad->ins_user != Auth::user()->user_id)
        <div class="row">
            <div class="ui raised segment">
                <h3 class="ui header">Send Message to Seller</h3>
                @if (count($errors) > 0)
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        There was some error/s with your submission
                    </div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('success'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <div class="header">
                        @if (session('success'))
                            Message Sent
                        @endif
                    </div>
                    {{ session('success') }}
                </div>
                @endif
                <form class="ui form" id="sendMessage" method="post" action="{{ url('/item/'.$ad->slug) }}">
                    {{ csrf_field() }}
                    <div class="ui error message"></div>
                    <div class="two fields">
                        <div class="required field {{ $errors->has('firstname') ? ' error' : '' }}">
                            <label>First Name</label>
                            <input placeholder="First Name" name="firstname" type="text" value="{{ old('firstname') }}">
                        </div>
                        <div class="required field {{ $errors->has('lastname') ? ' error' : '' }}">
                            <label>Last Name</label>
                            <input placeholder="Last Name" name="lastname" type="text" value="{{ old('lastname') }}">
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="required field {{ $errors->has('email') ? ' error' : '' }}">
                            <label>Email Address</label>
                            <input placeholder="Your Email Address" type="text" name="email">
                        </div>
                        <div class="required field {{ $errors->has('phone') ? ' error' : '' }}">
                            <label>Phone Number</label>
                            <input type="text" name="phone" placeholder="09221111111" value="{{ old('phone') }}">
                        </div>
                    </div>
                    <div class="required field {{ $errors->has('bodymail') ? ' error' : '' }}">
                        <label>Send Message</label>
                        <textarea name="bodymail"></textarea>
                    </div>
                    <div class="field">
                        <button class="ui button fluid" type="submit">Send Message</button>
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
</div>

<!-- Report Modal -->
<div class="ui modal" id="reportModal" >
  <div class="header">Report Ad</div>
  <div class="content">
    <p>The Ad has been reported. Please wait for the admin to review the report. Thank you for reporting.</p>
  </div>
  <div class="actions">
    <div class="ui positive button">OK</div>
  </div>
</div>

@section('js')

$("#ad-detail").owlCarousel({
    navigation : true, 
    slideSpeed : 300,
    paginationSpeed : 400,
    items : 1, 
    itemsDesktop : true,
    itemsDesktopSmall : true,
    itemsTablet: true,
    itemsMobile : true,
    autoPlay : true,
    stopOnHover : true,
});

$('.ui.button.contact').state({
    text: {
        inactive : '<i class="phone square icon"></i>Click to Show Phone Number',
        active   : '<i class="phone square icon"></i>{{$ad->phone}}'
    }
});

$('#saveAdFavorite').click(function(e){
    e.preventDefault();
    var $this=$(this);
    var ad_slug = $this.data('adSlug');

    if ($this.hasClass("favorited")){

        $.ajax({
            url: "/item/" + ad_slug + "/favorite/remove",
            type:'POST',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            success: function(){
                $this.removeClass("favorited")
                console.log('Removed from favorite');
            }
        }); 
    } else {
        $.ajax({
            url: "/item/" + ad_slug + "/favorite",
            type:'POST',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            success: function(){
                $this.addClass("favorited")
                console.log('Add favorite');
            }
        }); 
    }
});

$('#reportAd').click(function(e){
    e.preventDefault();
    var $this=$(this);
    var ad_slug = $this.data('adSlug');

    $.ajax({
        url: "/item/" + ad_slug + "/report",
        type:'POST',
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        success: function(){
            $('#reportModal').modal('show');
            console.log('Report Done');
        }
    }); 
});

$('.message .close')
.on('click', function() {
    $(this)
    .closest('.message')
    .transition('fade')
    ;
})
;


@if (!Auth::Guest())
$('#saveAdFavorite').state({
    text: {
        inactive : '@if($ad->ad_favorites->where('user_id',Auth::user()->user_id)->count() != 0)<i class="red heart icon"></i>Remove Ad as Favorite @else <i class="red empty heart icon"></i>Save Ad as Favorite @endif',
        active   : '@if($ad->ad_favorites->where('user_id',Auth::user()->user_id)->count() != 0)<i class="red empty heart icon"></i>Save Ad as Favorite @else <i class="red heart icon"></i>Remove Ad as Favorite  @endif'
    }
});
@endif

@stop

@endsection
