<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
    'domain' => env('MAILGUN_DOMAIN'),
    'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
    'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
    'key' => env('SES_KEY'),
    'secret' => env('SES_SECRET'),
    'region' => 'us-east-1',
    ],

    'paypal' => [
    'client_id' => 'AYdMx0YLcW23hOkflIYb1n7xD-fFaeoWDZihf4d7kWdEJblchqOJ9_fPU2Cxew3H-W3j6j7_9UvmTep8',
    'secret' => 'EIskpAepkjrtIt9GFh8kw_lEHY3k5mhKZvvrXveaaqn6bm-q81tyfUUPLPkcWFAqu1uG7_aC-8YW-Ye7'
    ],


    'sparkpost' => [
    'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
    'model' => App\User::class,
    'key' => env('STRIPE_KEY'),
    'secret' => env('STRIPE_SECRET'),
    ],

    ];
