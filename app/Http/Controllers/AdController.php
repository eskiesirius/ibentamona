<?php

namespace App\Http\Controllers;

use App\Ad;
use App\MainCategory;
use App\Location;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\Http\Request\SendMailRequest;
use App\Service\EmailUserService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class AdController extends Controller
{
    protected $emailUserService;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EmailUserService $emailUserService)
    {
        $this->middleware('auth',['except' => ['show','sendMail']]);
        $this->emailUserService = $emailUserService;
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('ad-post');
    }

    /**
    * Show Item Details.
    *
    * @return \Illuminate\Http\Response
    */
    public function show(Ad $ad)
    {	
        $adDetail = Ad::getApproveAds()->where('slug',$ad->slug)
        ->firstOrFail();

        //Increment View
        $adDetail->increment('total_views');

        //Get All Main Categories
        $maincategories = MainCategory::where('del_flg','0')->get();

        return view('ad-detail',[
            'ad' => $adDetail,
            'maincategories' => $maincategories,
            ]);
    }

    /**
     * Register Ad.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {	
        $ad_gallery = $request->ad_gallery;

        //Check File if exist
        $request->ad_gallery = $this->checkImageFiles($ad_gallery);

        $v = Validator::make($request->all(),[
            'ad_name' => 'required|max:100',
            'cat_id' => 'required',
            'city_id' => 'required',
            'ad_gallery' => 'required',
            'condition_code' => 'required',
            'price' => 'required|numeric|max:9999999999',
            'phone' => 'required|numeric|digits_between:11,11',
            'description' => 'required',
            ]);

        if ($v->fails())
        {
            return view('ad-post')->withErrors($v->errors());
        }

        $negotiable = 0;
        if ($request->negotiable != null)
            $negotiable = $request->negotiable;

        $ad = Ad::create([
            'ad_name' => $request->ad_name,
            'cat_id' => $request->cat_id,
            'sub_cat_id' => $request->sub_cat_id,
            'city_id' => $request->city_id,
            'ad_gallery' => $request->ad_gallery,
            'condition_code' => $request->condition_code,
            'price' => $request->price,
            'negotiable' => $negotiable,
            'phone' => $request->phone,
            'description' => $request->description,
            'tag' => $request->tag,
            'ins_user' => \Auth::user()->user_id,
            ]);

        $ad->save();

        flash()->overlay('Congratulations!','Please wait while the admin is reviewing your ad.');

        return redirect('posting');
    }

    public function edit(Ad $ad)
    {
        $adDetail = Ad::getNotDeletedAds()->where('slug',$ad->slug)
        ->where('ins_user',\Auth::user()->user_id)
        ->firstOrFail();

        $maincategory = MainCategory::where('del_flg','0')->get();
        $subcategory = SubCategory::where('del_flg','0')
                                    ->where('cat_id',$adDetail->cat_id)
                                    ->get();
        $citylist = Location::where('del_flg','0')->get();

        return view('ad-edit',[
            'ad' => $adDetail,
            'maincategorylist' => $maincategory,
            'subcategorylist' => $subcategory,
            'citylist' => $citylist,
            ]);
    }

    public function updatead(Request $request,$oldslug)
    {
        $ad_gallery = $request->ad_gallery;

        //Check File if exist
        $request->ad_gallery = $this->checkImageFiles($ad_gallery);

        $v = Validator::make($request->all(),[
            'ad_name' => 'required|max:100',
            'cat_id' => 'required',
            'city_id' => 'required',
            'ad_gallery' => 'required',
            'condition_code' => 'required',
            'price' => 'required|numeric|max:9999999999',
            'phone' => 'required|numeric|digits_between:11,11',
            'description' => 'required',
            ]);

        if ($v->fails())
        {
            return view('ad-edit')->withErrors($v->errors());
        }

        $adDetail = Ad::where('slug',$oldslug)
        ->GetNotDeletedAds()
        ->where('ins_user',\Auth::user()->user_id)
        ->firstOrFail();

        $adDetail->fill($request->all());

        $adDetail->save();

        if ($request->negotiable == null)
            $adDetail->update(['negotiable' => '0']);

        flash()->success('Ad Updated.','Your ad has been updated');
        return redirect()->route('my-ads');
    }

    public function removead(Request $request)
    {
        $ad = Ad::getNotDeletedAds()->where('slug',$request->slug);

        if (\Auth::user()->user_type != User::ADMIN_USER)
            $ad = $ad->where('ins_user',\Auth::user()->user_id);

        $ad = $ad->firstOrFail();

        $ad->del_flg = '1';
        $ad->save();

        flash()->success('Ad Removed','Your ad has been removed.');
        return redirect()->route('my-ads');
    }

    private function checkImageFiles($filenames)
    {
        $filenameArray = explode(',',$filenames);

        //Check file if exists if not remove element
        for ($index=0; $index < count($filenameArray); $index++) 
        { 
            if (!\File::exists('assets/img/ad/' . \Auth::user()->user_id.'/'.$filenameArray[$index]))
            {
                \Log::info('Not found '.$filenameArray[$index]);
               $filenameArray[$index] = '';
            }
        }

        //Remove blank elements
        $filenameArray = array_filter($filenameArray);

        return implode(",", $filenameArray);
    }

    /**
     * Upload Image.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {	
        $input = $request->all();

        //Validate image
        $rules = array(
            'file' => 'image|max:3000',
            );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return 'error';
        }

        $user = \Auth::user();
        $file = $request->file('file');
        $filename = uniqid() . $file->getClientOriginalName();

    	//Move File
        $file->move('assets/img/ad/' . $user->user_id,$filename);

        return $filename;
    }

    /**
     * Remove Uploaded Image.
     *
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {	
        $req = $request->all();

        $user = \Auth::user();

        $filename = $req['fileList'];
        \File::delete('assets/img/ad/' .$user->user_id.'/'.$filename);

        return $filename;
    }

    public function sendMail(Request $request,Ad $ad)
    {

        //validate
        $this->validate($request, [
            'email' => 'required|email',
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'phone' => 'required|numeric|digits_between:11,11',
            'bodymail' => 'required',
        ]);

        $this->emailUserService->sendUserEmail($request,$ad);

        return back()->withSuccess('Your message has been sent to the owner.');
    }
}
