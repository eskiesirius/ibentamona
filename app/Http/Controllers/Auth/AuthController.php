<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Service\ActivationService;
use Log;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $activationService;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->activationService = $activationService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [ 
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:6|confirmed',
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'phone' => 'required|numeric|digits_between:11,11',
            'gender' => 'required',
            'terms' => 'required',
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
            ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
                );
        }

        $user = $this->create($request->all());

        $this->activationService->sendActivationMail($user);

        flash()->overlay('Congratulations!','Please Check Your Email For the Confirmation Link.');

        return redirect('/login');
    }


    public function authenticated(Request $request, $user)
    {
        if ($user->del_flg == '1') {
            auth()->logout();
            return back()->with('negative', 'You may not able to access this account please contact the administrator.');
        }

        if ($user->status == '0') {
            $this->activationService->sendActivationMail($user);
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    //Clicking the link from the email
    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            return redirect('/login')->with('success', 'Your Account is now active, you can now log in using your account.');
        }
        abort(404);
    }
}
