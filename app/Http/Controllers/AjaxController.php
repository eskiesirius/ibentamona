<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Location;
use App\MainCategory;
use App\SubCategory;
use App\User;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function categoryList()
	{
		$maincat = MainCategory::where('del_flg','0')
		->select('cat_name as name','cat_id as value')
		->orderBy('cat_name','asc')
		->get();

		return response()->json([
			'success'=>true, 
			'results'=>$maincat
			]);
	}

	public function categoryListWithQuery($query)
	{
		$maincat = MainCategory::where('del_flg','0')
		->where('cat_name','like',$query.'%')
		->orderBy('cat_name','asc')
		->select('cat_name as name','cat_id as value')
		->get();

		return response()->json([
			'success'=>true, 
			'results'=>$maincat
			]);
	}

	public function subCategoryList($maincategory)
	{
		$subcat = SubCategory::where('del_flg','0')
		->where('cat_id',$maincategory)
		->orderBy('sub_cat_name','asc')
		->select('sub_cat_name as name','sub_cat_id as value')
		->get();

		return response()->json([
			'success'=>true, 
			'results'=>$subcat
			]);
	}

	public function cityList()
	{
		$cities = Location::where('del_flg','0')
		->select('city_name as name','city_id as value')
		->orderBy('city_name','asc')
		->get();

		return response()->json([
			'success'=>true, 
			'results'=>$cities
			]);
	}

	/**
	 * Approve Ad
	 * @param  string $slug
	 * @return json
	 */
	public function approveAd($slug)
	{
		if (\Auth::user()->user_type != User::ADMIN_USER)
		{
			return response()->json([
				'success' => false,
				'error' => 'Fucked Off!'
				],400);
		}

		$ad = Ad::getNotDeletedAds()->where('slug',$slug)->firstOrFail();

		$ad->status = '1';
		$ad->approved_date = Carbon::now();
		$ad->save();

		return response()->json([
			'success' => true,
			'message' => 'Ad Approved!'
			], 200);
	}

	/**
	 * Approve User
	 * @param  integer $id 
	 * @return json
	 */
	public function approveUser($id)
	{
		if (\Auth::user()->user_type != User::ADMIN_USER)
		{
			return response()->json([
				'success' => false,
				'error' => 'Fucked Off!'
				],400);
		}

		$user = User::getNotDeletedUsers()->where('user_id',$id)->firstOrFail();

		$user->status = '1';
		$user->save();

		return response()->json([
			'success' => true,
			'message' => 'User Approved!'
			], 200);
	}
}
