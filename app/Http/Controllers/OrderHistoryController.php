<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderHistory;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderHistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display Order History data
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    	$orders = OrderHistory::where('del_flg','0');

    	if (\Auth::user()->user_type != User::ADMIN_USER)
    		$orders = $orders->where('user_id',\Auth::user()->user_id);

    	$orders = $orders->paginate(20);

    	return view('order_histories',['orders' => $orders]);
    }
}
