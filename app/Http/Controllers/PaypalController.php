<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OrderHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Paypal;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use Redirect;

class PaypalController extends Controller
{
	private $_apiContext;

	public function __construct()
	{
		$this->middleware('auth');
		$this->_apiContext = PayPal::ApiContext(
			config('services.paypal.client_id'),
			config('services.paypal.secret'));

		$this->_apiContext->setConfig(array(
			'mode' => 'sandbox',
			'service.EndPoint' => 'https://api.sandbox.paypal.com',
			'http.ConnectionTimeOut' => 30,
			'log.LogEnabled' => true,
			'log.FileName' => storage_path('logs/paypal.log'),
			'log.LogLevel' => 'FINE'
			));
	}

	public function getCheckout(Request $request)
	{
		$ad = Ad::getApproveAds()->whereSlug($request->slug)->firstOrFail();

		$payer = PayPal::Payer();
		$payer->setPaymentMethod('paypal');

		$item_1 = new Item();
	    $item_1->setName('Feature For 7 Days: '.$ad->ad_name) // item name
	    ->setCurrency('PHP')
	    ->setQuantity(1)
        ->setPrice('150'); // unit price

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = PayPal:: Amount();
        $amount->setCurrency('PHP');
        $amount->setTotal(150); 

        $invoicenumber = uniqid();
        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($item_list);
        $transaction->setInvoiceNumber($invoicenumber);
        $transaction->setDescription('Feature Ad');

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl(url('/panel/feature/return'));
        $redirectUrls->setCancelUrl(url('/panel/feature/cancel'));

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;

        $this->createOrder($ad,$invoicenumber,$response);
		
        return redirect($redirectUrl);
    }

    private function createOrder($ad,$invoicenumber,$response)
    {
    	return OrderHistory::create([
            'invoice_number' => $invoicenumber,
            'payment_id' => $response->id,
            'package_id' => '1',
            'ad_id' => $ad->ad_id,
            'status' => '0',
        	'user_id' => \Auth::user()->user_id,
            ]);
    }

    public function getDone(Request $request)
    {
    	$id = $request->get('paymentId');
    	$token = $request->get('_token');
    	$payer_id = $request->get('PayerID');

    	$payment = PayPal::getById($id, $this->_apiContext);

    	$paymentExecution = PayPal::PaymentExecution();

    	$paymentExecution->setPayerId($payer_id);
    	$result = $payment->execute($paymentExecution, $this->_apiContext);

    	$order = OrderHistory::where('payment_id',$result->id)->firstOrFail();
    	$order->status = '1';
    	$order->approved_date = Carbon::now();
    	$order->save();

    	$ad = Ad::getApproveAds()->where('ad_id',$order->ad_id)->firstOrFail();

        if ($this->isFeaturedAlready($ad->status))
            $ad->feat_expiration = $ad->feat_expiration->addDays(7);
        else 
        {
            $ad->feat_flg = '1';
            $ad->feat_expiration = Carbon::now()->addDays(7);
        }
    	
    	$ad->save();

		flash()->overlay('Purchase Completed!','Thank you for purchasing our package for more info you can check your order history.');
    	return redirect()->route('my-ads');
    }

    private function isFeaturedAlready($featured)
    {
        return $featured == OrderHistory::FEATURED_AD;
    }

    public function getCancel()
    {
		flash()->overlay('Purchase Cancelled!','You can still purchase our package by Order again or continue the previous transaction by checking your order history then get the invoice number and deposit the money in the bank. Dont forget to mention the invoice number.');
    	return redirect()->route('my-ads');
    }
}
