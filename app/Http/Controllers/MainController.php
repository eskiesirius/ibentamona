<?php

namespace App\Http\Controllers;

use App\Ad;
use App\MainCategory;
use App\SubCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MainController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {

     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //Get All Main Categories
        $maincategories = MainCategory::where('del_flg','0')->get();

        //Get All Featured
        $featuredads = Ad::getApproveAds()
        ->where('feat_flg','1')
        ->orderBy('ad_id', 'RAND')
        ->get();

        //Get Recent Ads
        $recentads = Ad::getApproveAds()
        ->orderBy('approved_date', 'desc')
        ->take(3)
        ->get();

        //Get Popular Ads
        $popularads = Ad::getApproveAds()
        ->orderBy('total_views', 'desc')
        ->take(3)
        ->get();

        return view('main', [
            'maincategories' => $maincategories,
            'featuredads' => $featuredads,
            'recentads' => $recentads,
            'popularads' => $popularads,
            ]);
    }

    /**
     * Search Ad
     * 
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        //Get All Main Categories
        $maincategories = $this->getAllMainCategory();

        //Get All Sub Categories
        $subcategories = $this->getAllSubCategory();

        //Get all Ads
        $ads = Ad::getapproveads()
            ->orderBy('feat_flg','desc')
            ->latest('approved_date');

        $ads = $this->appendTitleQuery($request,$ads);
        $ads = $this->appendMainCategoryQuery($request,$ads);
        $ads = $this->appendSubCategoryQuery($request,$ads);
        $ads = $this->appendConditionQuery($request,$ads);
        $ads = $this->appendPriceQuery($request,$ads);

        $ads = $ads->paginate(15);

        return view('search-detail',[
            'maincategories' => $maincategories,
            'subcategories' => $subcategories,
            'ads' => $ads,
            'parameters' => $request,
            ]);
    }

    /**
     * Get all list of Main Category
     * 
     * @return Collection of Main Category
     */
    public function getAllMainCategory()
    {
        return MainCategory::getActiveCategory()->get();
    }

    /**
     * Get all list of Sub Category
     * 
     * @return Collection of Sub Category
     */
    public function getAllSubCategory()
    {
        return SubCategory::getActiveSubCategory()->get();
    }

    /**
     * Remove Comma from the string
     * 
     * @param string
     */
    public function removeComma($word)
    {
        return str_replace(',','',$word);
    }

    /**
     * Get query for Ttile
     * 
     * @param  Request $request, Ad $ads
     * @return Query
     */
    public function appendTitleQuery($request,$ads)
    {
        if (!$request->has('title')) return $ads;
        return $ads->where('ad_name','LIKE',"%$request->title%");
    }

    /**
     * Get query for Main Category
     * 
     * @param  Request $request, Ad $ads
     * @return Query
     */
    public function appendMainCategoryQuery($request,$ads)
    {
        if (!$request->has('category')) return $ads;

        $id = MainCategory::getActiveCategory()->where('slug',$request->category)->first();

        if($id == null) return $ads; //If not found return ads

        return $ads->where('cat_id',$id->cat_id);
    }

    /**
     * Get query for Sub Category
     * 
     * @param  Request $request, Ad $ads
     * @return Query
     */
    public function appendSubCategoryQuery($request,$ads)
    {
        if (!$request->has('subcategory')) return $ads;
        
        $id = SubCategory::getActiveCategory()->where('slug',$request->subcategory)->first();

        if($id == null) return $ads; //If not found return ads

        return $ads->where('sub_cat_id',$id->sub_cat_id);
    }

    /**
     * Get query for Condition Code
     * 
     * @param  Request $request, Ad $ads
     * @return Query
     */
    public function appendConditionQuery($request,$ads)
    {
        if (!$request->has('condition')) return $ads;
        
        return $ads->whereIn('condition_code',$request->condition);
    }

    /**
     * Get query for Price
     * 
     * @param  Request $request, Ad $ads
     * @return Query
     */
    public function appendPriceQuery($request,$ads)
    {
        if ($request->has('min') && $request->has('max') )
            return $ads->whereBetween('price',[$this->removeComma($request->min),$this->removeComma($request->max)]);  
        else if ($request->has('min'))
            return $ads->where('price','>=',$this->removeComma($request->min)); //minimum of
        else if ($request->has('max'))
            return $ads->where('price','<=',$this->removeComma($request->max)); //maximum of

        return $ads;
    }
}
