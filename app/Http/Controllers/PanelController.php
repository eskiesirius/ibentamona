<?php

namespace App\Http\Controllers;

use App\Ad;
use App\FavoriteAd;
use App\User;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class PanelController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the My Ads.
    *
    * @return \Illuminate\Http\Response
    */
    public function myads()
    {
        if (\Auth::user()->user_type == User::ADMIN_USER)
            $ads = Ad::getNotDeletedAds()->paginate(10);
        else
            $ads = Ad::where('ins_user',\Auth::user()->user_id)->getNotDeletedAds()->paginate(10);

        return view('user-panel.partials.my-ads',['ads' => $ads]);
    }

    /**
    * Show the Favorite Ads.
    *
    * @return \Illuminate\Http\Response
    */
    public function favoriteads()
    {
        $ads = FavoriteAd::where('user_id',\Auth::user()->user_id)->paginate(10);

        return view('user-panel.partials.favorite-ads',['ads' => $ads]);
    }

    /**
    * Remove the Favorite Ad.
    *
    * @return \Illuminate\Http\Response
    */
    public function removefavoriteads(Request $request)
    {
        $ads = FavoriteAd::where('user_id',\Auth::user()->user_id)
                            ->where('ad_id',$request->ad_id)
                            ->delete();

        flash()->success('Ad Removed.','Ad has been removed in your favorites');
        return redirect()->route('favorite-ad');
    }

    /**
    * Show profile.
    *
    * @return \Illuminate\Http\Response
    */
    public function showprofile()
    {
        return view('user-panel.profile',['user' => \Auth::user()]);
    }

    /**
    * Update profile
    *
    * @return \Illuminate\Http\Response
    */
    public function updateprofile(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'phone' => 'required|numeric|digits_between:11,11',
            'gender' => 'required',
            'file' => 'image|max:3000',
            ]); 

        $user = \Auth::user();
        $oldphoto = $user->profile_photo;
        $user->fill($request->all());

        $file = $request->file('file');
        if ($file != null)
        {
            $fullpath = 'assets/img/profile_photo/' . $user->user_id.'/';
            $filename = uniqid() . $file->getClientOriginalName();

            \File::cleanDirectory($fullpath);

            $file->move($fullpath,$filename);
            $user->profile_photo = $fullpath.$filename;
        } else 
        $user->profile_photo = $oldphoto;

        $user->save();

        flash()->success('User Information Updated.','Your profile has been updated');
        return redirect('/panel/profile');
    }

    /**
    * Update Password
    *
    * @return \Illuminate\Http\Response
    */
    public function updatepassword(Request $request)
    {
        $user = \Auth::user();

        $validator = Validator::make($request->all(),[
            'password' => 'required|min:6|different:current_password|confirmed',
            'current_password' => 'required',
            ]);

        if($validator->fails() )
        {
            flash()->error('There was some error/s with your submission.','Please Check it again.');
            return redirect('/panel/profile')->withErrors($validator->errors(),'PasswordError');
        }

        if (!\Hash::check($request->current_password,$user->password))
        {
            flash()->error('There was some error/s with your submission.','Please Check it again.');
            return redirect('/panel/profile')->withErrors(['current_password' => 'Incorrect Current Password'],'PasswordError');
        }

        $user->password = bcrypt($request->password);
        $user->save();

        flash()->success('Password Updated.','Your password has been updated');
        return redirect('/panel/profile');
    }

    /**
    * Show All Users.
    *
    * @return \Illuminate\Http\Response
    */
    public function showusers()
    {
        if (\Auth::user()->user_type != User::ADMIN_USER)
            return redirect('/');

        $users = User::getNotDeletedUsers()->paginate(15);

        return view('user-panel.users',['users' => $users]);
    }

    /**
     * Remove User and Ad
     * @param  Request $request 
     * @return \Illuminate\Http\Response
     */
    public function removeuser(Request $request)
    {
        if (\Auth::user()->user_type != User::ADMIN_USER)
            return redirect('/');

        $user = User::getNotDeletedUsers()->where('user_id',$request->user_id)->firstOrFail();
        $user->del_flg = '1';
        $user->save();

        $user->ads()->delete();

        flash()->success('User Removed.','User has been removed.');

        return redirect()->route('users');
    }
}
