<?php

use App\Ad;
use App\FavoriteAd;
use App\Location;
use App\MainCategory;
use App\ReportAd;
use App\SubCategory;
use App\User;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('user/activation/{confirmation_key}', 'Auth\AuthController@activateUser')->name('user.status');

Route::group(['middleware' => 'web'], function () {

	Route::get('/','MainController@index');

	//Fetch All Main Category List
	Route::get('/category-list', 'AjaxController@categoryList');

	//Fetch Main Category List based on users input
	Route::get('/category-list/{query}', 'AjaxController@categoryListWithQuery');

	//Fetch Sub Category List based on Main Category
	Route::get('/sub-category-list/{maincategory}', 'AjaxController@subCategoryList');

	//Fetch All City List
	Route::get('/city-list', 'AjaxController@cityList');

	//Set Ad as Favorite
	Route::post('/item/{ad}/favorite', function($ad){
		$ad = Ad::where('slug',$ad)
		->where('ins_user','<>',\Auth::user()->user_id)
		->firstOrFail();

		$favoritead = FavoriteAd::create([
			'ad_id' => $ad->ad_id,
			'user_id' => \Auth::user()->user_id,
			]);

		$favoritead->save();
	});

	//Remove Ad as Favorite
	Route::post('/item/{ad}/favorite/remove', function($ad){
		$ad = Ad::where('slug',$ad)->firstOrFail();

		$favoritead = FavoriteAd::where('ad_id',$ad->ad_id)
		->where('user_id',\Auth::user()->user_id)
		->delete();
	});

	//Report Ad
	Route::post('/item/{ad}/report', function($ad){
		$ad = Ad::where('slug',$ad);

		if (!\Auth::guest())
			$ad = $ad->where('ins_user','<>',\Auth::user()->user_id);
		
		$ad = $ad->firstOrFail();

		$reportad = ReportAd::firstOrCreate(['ad_id' => $ad->ad_id]);

		if ($reportad != null)
		{
			$reportad->increment('report_count');
		}

		return Response::json(array(
			'success' => true,
			'message' => 'Report Done!'

			), 200);
	});

	//Approve Ad
	Route::post('/item/{ad}/approve', 'AjaxController@approveAd');

	Route::post('/posting','AdController@register');

	Route::get('/item/{ad}','AdController@show')->name('ad-detail');

	Route::get('/item/{ad}/edit','AdController@edit');

	Route::patch('/item/{ad}/edit','AdController@updatead');

	Route::post('/item/remove','AdController@removead')->name('remove-ad');

	Route::get('/item','MainController@search')->name('search-ads');

	Route::get('/posting','AdController@index');

	Route::post('/upload/ad_image','AdController@upload');

	Route::post('/remove/ad_image','AdController@remove');

	Route::get('/panel/my-ads', 'PanelController@myads')->name('my-ads');

	Route::get('/panel/favorite-ads', 'PanelController@favoriteads')->name('favorite-ad');

	Route::post('/panel/favorite-ads/remove', 'PanelController@removefavoriteads')->name('remove-fave-ad');

	Route::get('/panel/profile', 'PanelController@showprofile');

	Route::get('/panel/users', 'PanelController@showusers')->name('users');

	Route::patch('/panel/profile', 'PanelController@updateprofile');

	Route::patch('/panel/profile/changepassword', 'PanelController@updatepassword');

	Route::post('/panel/feature', 'PaypalController@getCheckout')->name('checkout');

	Route::get('/panel/feature/return', 'PaypalController@getDone');

	Route::get('/panel/feature/cancel', 'PaypalController@getCancel');

	Route::get('/panel/order-histories', 'OrderHistoryController@show')->name('order-histories');

	Route::post('/user/{id}/approve', 'AjaxController@approveUser');

	Route::post('/panel/user/remove', 'PanelController@removeuser')->name('remove-user');

	Route::post('/item/{ad}','AdController@sendMail')->name('sendUserEmail');

	Route::get('/user/{id}',function($id){
		$user = User::where('user_id',$id)->where('del_flg','0')->firstOrFail();

		return view('profile',[
			'user' => $user,
			'ads' => $user->ads->where('status','1'),
			]);
	});
});
