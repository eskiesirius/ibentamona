<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = array('sub_cat_name','cat_id','slug','del_flg');

    protected $table = 'sub_categories';

    protected $primaryKey = 'sub_cat_id';

    public function main_category() {
        return $this->belongsTo('Main_Category');
    }

    public function Ads()
    {
    	return $this->hasMany('App\Ad','sub_cat_id');
    }

    public function scopeGetActiveSubCategory($query)
    {
        return $query->where('del_flg','0');
    }
}
