<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportAd extends Model
{
	protected $fillable = array('ad_id','report_count','del_flg');

	protected $primaryKey = 'ad_id';

    protected $table = 'report_ads';
}
