<?php
namespace App\Repository;

use Illuminate\Database\Connection;
use Log;
use DB;

class ActivationRepository
{

    protected $db;

    protected $table = 'users';

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    //Create Random Token
    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    //Create activation
    public function createActivation($user)
    {

        $activation = $this->getActivation($user);

        if (!$activation) {
            return $this->createToken($user);
        }
        return $this->regenerateToken($user);

    }

    //Generate another token
    private function regenerateToken($user)
    {

        $token = $this->getToken();

        $this->db->table($this->table)->where('user_id', $user->user_id)->update([
            'confirmation_key' => $token,
        ]);
        return $token;
    }

    private function createToken($user)
    {
        $token = $this->regenerateToken($user);
        return $token;
    }

    public function getActivation($user)
    {
        return $this->db->table($this->table)->where('user_id', $user->id)->first();
    }


    public function getActivationByToken($token)
    {
        return $this->db->table($this->table)->where('confirmation_key', $token)->first();
    }

    public function deleteActivation($token)
    {
        $this->db->table($this->table)->where('confirmation_key', $token)->update([
            'confirmation_key' => '',
        ]);;
    }

}