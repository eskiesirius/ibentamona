<?php

namespace App\Pagination;

use Illuminate\Pagination\UrlWindow;
use Illuminate\Support\HtmlString;
use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Contracts\Pagination\Presenter as PresenterContract;
use Illuminate\Pagination\BootstrapThreeNextPreviousButtonRendererTrait;
use Illuminate\Pagination\UrlWindowPresenterTrait;

/**
* Creating Custom Presenter for our Pagination
*/
class CustomPresenter implements PresenterContract
{
	use UrlWindowPresenterTrait;

	protected $paginator;

	protected $window;
	
	/**
     * Create a new Bootstrap presenter instance.
     *
     * @param  \Illuminate\Contracts\Pagination\Paginator  $paginator
     * @param  \Illuminate\Pagination\UrlWindow|null  $window
     * @return void
     */
    public function __construct(PaginatorContract $paginator, UrlWindow $window = null)
    {
        $this->paginator = $paginator;
        $this->window = is_null($window) ? UrlWindow::make($paginator) : $window->get();
    }

    /**
     * Convert the URL window into Semantic UI HTML.
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function render()
    {
        if ($this->hasPages()) {
            return new HtmlString(sprintf(
                '<div class="ui pagination menu">%s %s %s</div>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            ));
        }

        return '';
    }

    /**
     * Determine if the underlying paginator being presented has pages to show.
     *
     * @return bool
     */
    public function hasPages()
    {
        return $this->paginator->hasPages();
    }

    /**
     * Get HTML wrapper for disabled text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getDisabledTextWrapper($text)
    {
        return '<a class="item disabled">'.$text.'</a>';
    }

    /**
     * Get HTML wrapper for active text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getActivePageWrapper($text)
    {
        return '<a class="item active">'.$text.'</a>';
    }

    /**
     * Get HTML wrapper for an available page link.
     *
     * @param  string  $url
     * @param  int  $page
     * @param  string|null  $rel
     * @return string
     */
    protected function getAvailablePageWrapper($url, $page, $rel = null)
    {
        $rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

        return '<a class="item" href="'.htmlentities($url).'"'.$rel.'>'.$page.'</a>';
    }

    /**
     * Get a pagination "dot" element.
     *
     * @return string
     */
    protected function getDots()
    {
        return $this->getDisabledTextWrapper('...');
    }

    /**
     * Get HTML wrapper for disabled arrow.
     *
     * @param  string  $text
     * @return string
     */
    protected function getDisabledArrowTextWrapper($arrow)
    {
        return '<a class="item icon disabled"><i class="'.$arrow.' chevron icon"></i></a>';
    }

    /**
     * Get HTML wrapper for a page link.
     *
     * @param  string  $url
     * @param  int  $page
     * @param  string|null  $rel
     * @return string
     */
    protected function getPageLinkArrowWrapper($url, $arrow , $rel = null)
    {
        return $this->getAvailableArrowPageWrapper($url,$arrow,$rel);
    }

    /**
     * Get HTML wrapper for an available arrow.
     *
     * @param  string  $url
     * @param  string  $arrow
     * @param  string|null  $rel
     * @return string
     */
    protected function getAvailableArrowPageWrapper($url, $arrow ,$rel = null)
    {
        $rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

        return '<a class="item icon" href="'.htmlentities($url).'"'.$rel.'><i class="'.$arrow.' chevron icon"></i></a>';
    }

    /**
     * Get the previous page pagination element.
     *
     * @return string
     */
    public function getPreviousButton()
    {
        // If the current page is less than or equal to one, it means we can't go any
        // further back in the pages, so we will render a disabled previous button
        // when that is the case. Otherwise, we will give it an active "status".
        if ($this->paginator->currentPage() <= 1) {
            return $this->getDisabledArrowTextWrapper('left');
        }

        $url = $this->paginator->url(
            $this->paginator->currentPage() - 1
        );

        return $this->getPageLinkArrowWrapper($url,'left','prev');
    }

    /**
     * Get the next page pagination element.
     *
     * @return string
     */
    public function getNextButton()
    {
        // If the current page is greater than or equal to the last page, it means we
        // can't go any further into the pages, as we're already on this last page
        // that is available, so we will make it the "next" link style disabled.
        if (! $this->paginator->hasMorePages()) {
            return $this->getDisabledArrowTextWrapper('right');
        }

        $url = $this->paginator->url($this->paginator->currentPage() + 1);

        return $this->getPageLinkArrowWrapper($url, 'right', 'next');
    }
}