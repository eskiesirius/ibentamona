<?php

namespace App\Service;

use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Log;

class EmailUserService
{

    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    //Send Activation Email
    public function sendUserEmail($request,$ad)
    {
        $link = route('sendUserEmail', $ad->slug);
        $message = sprintf('%s

Name: %s
Phone Number:%s
Email: %s

Note: Please Do not reply in this message
This message sent from %s', $request->bodymail,$request->firstname. ' '.$request->lastname,$request->phone, $request->email,$link);

        
        $this->mailer->raw($message, function (Message $m) use ($request,$ad) {
            Log::info($request->email);
            $m->to($ad->user->email)->subject('Message from someone');
        });
    }
}