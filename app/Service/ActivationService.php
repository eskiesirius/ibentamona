<?php

namespace App\Service;

use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use App\Repository\ActivationRepository;
use Log;
use App\User;

class ActivationService
{

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer, ActivationRepository $activationRepo)
    {
        $this->mailer = $mailer;
        $this->activationRepo = $activationRepo;
    }

    //Send Activation Email
    public function sendActivationMail($user)
    {
        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user);
        
        $link = route('user.status', $token);
        $message = sprintf('Activate account <a href="%s">%s</a>', $link, $link);
  
        $this->mailer->raw($message, function (Message $m) use ($user) {
            $m->to($user->email)->subject('Activation mail');
        });


    }

    //Activate Account when Clicking the activation from mail
    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->status = '1';
        $user->confirmation_date = new Carbon();
        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }

    //Send activation mail again
    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}