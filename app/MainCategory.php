<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    protected $fillable = array('cat_name', 'semantic_icon','slug', 'cat_image','del_flg');

    protected $table = 'main_categories';

    protected $primaryKey = 'cat_id';

    //Change url name to Slug
    public function getRouteKeyName() {
        return 'cat_name';
    }

    public function Ads()
    {
        return $this->hasMany('App\Ad','cat_id');
    }

    //One Category with Many Sub category
    public function sub_categories() {
        return $this->hasMany('App\SubCategory','cat_id');
    }

    public function scopeGetActiveCategory($query)
    {
        return $query->where('del_flg','0');
    }
}
