<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = array('city_name','slug','del_flg');

    protected $primaryKey = 'city_id';
}
