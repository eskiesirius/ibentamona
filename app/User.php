<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    CONST ADMIN_USER = '0';
    CONST NORMAL_USER = '1';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','email', 'lastname', 'password', 'phone','gender','profile_photo','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $primaryKey = 'user_id';

    protected $dates = ['last_login'];

    //One user many ads
    public function ads()
    {
        return $this->hasMany('App\Ad','ins_user','user_id');
    }

    //One user many favorite ads
    public function fave_ads()
    {
        return $this->hasMany('App\FavoriteAd');
    }

    public function scopeGetNotDeletedUsers($query)
    {
        return $query->where('del_flg','0');
    }
}
