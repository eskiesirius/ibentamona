<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteAd extends Model
{
    protected $fillable = array('ad_id','user_id','del_flg');

    protected $table = 'favorite_ads';

    public function favoriteads() {
        return $this->belongsToMany('App\Ad','Users','user_id','user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function ad()
    {
        return $this->belongsTo('App\Ad','ad_id');
    }
}
