<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class Ad extends Model
{
	protected $guarded = array('created_at', 'updated_at');

	protected $primaryKey = 'ad_id';

	protected $dates = ['approved_date','ad_expiration','feat_expiration'];

	//Change url name to Slug
    public function getRouteKeyName() {
        return 'slug';
    }

	public function setAdNameAttribute($value)
	{
		$this->attributes['ad_name'] = $value;
		
		$this->setUniqueSlug($value, '');
	}

	protected function setUniqueSlug($title, $extra)
	{
		$slug = str_slug($title.'-'.$extra);

		if (static::whereSlug($slug)->exists() && $this->isDirty('ad_name')) {
			$this->setUniqueSlug($title, $extra + 1);
			return;
		}

		$this->attributes['slug'] = $slug;
	}

	public function scopeGetNotDeletedAds($query)
	{
		return $query->where('del_flg','0');
	}

	public function scopeGetApproveAds($query)
	{
		return $query->GetNotDeletedAds()->where('status','1');
	}

	public function user()
    {
        return $this->belongsTo('App\User','ins_user','user_id');
    }

    public function category(){
        return $this->belongsTo('App\MainCategory','cat_id');
    }

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','sub_cat_id');
    }

    public function city(){
        return $this->belongsTo('App\Location','city_id');
    }

    public function ad_favorites(){
    	return $this->hasMany('App\FavoriteAd','ad_id');
    }
    
}
