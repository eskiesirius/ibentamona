<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    Const FEATURED_AD = '1';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_id','invoice_number','package_id','ad_id','status','user_id','del_flg'
    ];

    protected $dates = ['approved_date'];

    /**
     * Get Ad Name from Ad ID
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ad()
    {
    	return $this->belongsTo('App\Ad','ad_id');
    }
}
