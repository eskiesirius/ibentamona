<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('email',50)->unique();
            $table->string('firstname',50);
            $table->string('lastname',50);
            $table->string('user_type',1)->default('1');
            $table->string('gender',1);
            $table->string('profile_photo')->default(' ');
            $table->string('address',100)->default(' ');
            $table->string('phone',15);
            $table->string('password');
            $table->rememberToken();
            $table->string('confirmation_key')->default(' ');
            $table->dateTime('confirmation_date')->nullable();
            $table->string('status',1)->default('0'); 
            $table->dateTime('last_login');  
            $table->timestamps();
            $table->string('del_flg',1)->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
