<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('ad_id'); 
            $table->string('ad_name',100);
            $table->string('slug')->unique();
            $table->integer('cat_id');
            $table->integer('sub_cat_id');
            $table->integer('city_id');
            $table->string('condition_code',1)->default('0');
            $table->decimal('price',11,2);
            $table->string('negotiable',1)->default('0');
            $table->string('phone',15);
            $table->string('address',100);
            $table->text('ad_gallery');
            $table->text('description');
            $table->text('tag');
            $table->string('status',1)->default('0');
            $table->date('approved_date');
            $table->date('ad_expiration');
            $table->string('feat_flg',1)->default('0');
            $table->date('feat_expiration');
            $table->integer('total_views')->default(0);
            $table->integer('ins_user');
            $table->timestamps();
            $table->string('del_flg',1)->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
