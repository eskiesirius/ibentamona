<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_id')->unique();
            $table->string('invoice_number')->unique();
            $table->string('package_id');
            $table->integer('ad_id');
            $table->string('status',1);
            $table->string('user_id');
            $table->dateTime('approved_date')->nullable();
            $table->timestamps();
            $table->string('del_flg',1)->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_histories');
    }
}
