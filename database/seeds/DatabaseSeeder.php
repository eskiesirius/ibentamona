<?php

use Illuminate\Database\Seeder;
use App\MainCategory;
use App\SubCategory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Eloquent::unguard();

        $this->call('UserAppSeeder');
        $this->command->info('User app seeds finished.');

    	$this->call('CategoryAppSeeder');
    	$this->command->info('Category app seeds finished.');

        $this->call('AdTableSeeder');
        $this->command->info('Ads app seeds finished.');

        $this->call('LocationTableSeeder');
        $this->command->info('Location app seeds finished.');

        $this->call('FavoriteTableSeeder');
        $this->command->info('Favorite Ad app seeds finished.');
    }
}

class UserAppSeeder extends Seeder {
    public function run() {
        App\User::truncate();

        factory(App\User::class, 20)->create();

        $this->command->info('User done!');
    }
}

class CategoryAppSeeder extends Seeder {
    public function run() {
        App\MainCategory::truncate();
        App\SubCategory::truncate();

        factory(App\MainCategory::class, 20)->create();

        $this->command->info('The Main Category done!');

        factory(App\SubCategory::class, 20)->create();

        $this->command->info('The Sub Category combine done!');
    }
}

class AdTableSeeder extends Seeder {
    public function run() {
        App\Ad::truncate();

        factory(App\Ad::class, 20)->create();

        $this->command->info('Ads done!');
    }
}

class LocationTableSeeder extends Seeder {
    public function run() {
        App\Location::truncate();

        factory(App\Location::class, 20)->create();

        $this->command->info('Locations done!');
    }
}

class FavoriteTableSeeder extends Seeder {
    public function run() {
        App\FavoriteAd::truncate();

        factory(App\FavoriteAd::class, 20)->create();

        $this->command->info('Favorite Ads done!');
    }
}
