<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
	return [
	'firstname' => $faker->firstName,
	'lastname' => $faker->lastName,
	'gender' => $faker->randomElement($array = array ('1','2')),
	'phone' => $faker->numerify('0922#######'),
	'email' => $faker->safeEmail,
	'password' => bcrypt('123456'),
	'remember_token' => str_random(10),
	];
});

$factory->define(App\Ad::class, function ($faker) {
	return [
	'ad_name' => $title = $faker->sentence(mt_rand(3, 6)),
	'slug' => str_slug($title, "-"),
	'cat_id' => $faker->numberBetween($min = 1, $max = 20),
	'sub_cat_id' => $faker->numberBetween($min = 1, $max = 20),
	'city_id' => $faker->numberBetween($min = 1, $max = 20),
	'condition_code' => $faker->randomElement($array = array ('1','2')),
	'price' => $faker->numberBetween($min = 100, $max = 5000),
	'negotiable' => $faker->randomElement($array = array ('1','2')),
	'phone' => $faker->numerify('0922#######'),
	'address' => $faker->address,
	'ad_gallery' => 'elyse.png',
	'description' => join("\r\n", $faker->paragraphs(mt_rand(1, 3))),
	'tag' => 'tags',
	'status' =>  $faker->numberBetween($min = 0, $max = 1),
	'approved_date' => $faker->dateTimeBetween('-1 month', '+3 days'),
	'ad_expiration' => $faker->dateTimeBetween($startDate = "now", $endDate = "30 days")->format('Y-m-d'),
	'feat_flg' => $faker->randomElement($array = array ('0','1')),
	'feat_expiration' => $faker->dateTimeBetween($startDate = "now", $endDate = "30 days")->format('Y-m-d'),
	'total_views' => $faker->numberBetween($min = 1, $max = 5000),
	'ins_user' => $faker->numberBetween($min = 1, $max = 20),
	'del_flg' => '0',
	];
});

$factory->define(App\Location::class, function ($faker) {
	return [
	'city_name' => $name = $faker->city,
	'slug' => str_slug($name, "-"),
	'del_flg' => '0',
	];
});

$factory->define(App\MainCategory::class, function ($faker) {
	return [
	'cat_name' => $name = $faker->words(3,true),
	'cat_image' => 'elyse.png',
	'slug' => str_slug($name, "-"),
	'del_flg' => '0',
	];
});

$factory->define(App\SubCategory::class, function ($faker) {
	return [
	'cat_id' => $faker->numberBetween($min = 1, $max = 20),
	'sub_cat_name' => $name = $faker->words(3,true),
	'slug' => str_slug($name, "-"),
	'del_flg' => '0',
	];
});

$factory->define(App\FavoriteAd::class, function ($faker) {
	return [
	'ad_id' => $faker->numberBetween($min = 1, $max = 20),
	'user_id' => $faker->numberBetween($min = 1, $max = 20),
	'del_flg' => '0',
	];
});
